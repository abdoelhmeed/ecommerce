﻿using Ecommerce.Application.Dtos.SubcategoryDtos;

namespace Ecommerce.Application.Features.SubcategoryFeature.Commands.Update
{
    public record UpdateSubcategoryCommand(Guid? UserId, Guid? CompanyId, Guid Id, UpdateSubcategoryDto body) : IRequest<Response<SubcategoryDto>>;


}
