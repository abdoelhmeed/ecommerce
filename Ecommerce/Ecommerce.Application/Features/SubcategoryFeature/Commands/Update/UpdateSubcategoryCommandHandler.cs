﻿using Ecommerce.Application.Dtos.SubcategoryDtos;

namespace Ecommerce.Application.Features.SubcategoryFeature.Commands.Update
{
    public class UpdateSubcategoryCommandHandler : IRequestHandler<UpdateSubcategoryCommand, Response<SubcategoryDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdateSubcategoryCommandHandler> _logger;

        public UpdateSubcategoryCommandHandler(AppDbContext context, IMapper mapper, ILogger<UpdateSubcategoryCommandHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<SubcategoryDto>> Handle(UpdateSubcategoryCommand request, CancellationToken cancellationToken)
        {
            Subcategory category = await _context.Subcategories.FindAsync(request.Id) ?? throw new NotFoundException("Category Not Found");

            category.UpdateTime = DateTime.UtcNow;
            category.UpdateBy = request.UserId;
            category.NameAr = request.body.NameAr;
            category.NameEn = request.body.NameEn;
            category.DescriptionEn = request.body.DescriptionEn;
            category.DescriptionAr = request.body.DescriptionAr;
            if (category.Image != null)
                category.Image = request.body.Image;


            _context.Subcategories.Update(category);

            var updated = await _context.SaveChangesAsync();

            if (updated > 0)
                return new Response<SubcategoryDto>
                {
                    Data = _mapper.Map<SubcategoryDto>(category),
                    HttpStatusCode = HttpStatusCode.OK,
                    Succeeded = true,
                };


            return new Response<SubcategoryDto>
            {
                Data = _mapper.Map<SubcategoryDto>(category),
                HttpStatusCode = HttpStatusCode.BadRequest,
                Succeeded = true,
            };


        }
    }
}
