﻿using Ecommerce.Application.Dtos.SubcategoryDtos;

namespace Ecommerce.Application.Features.SubcategoryFeature.Commands.Create
{
    public record CreateSubcategoryCommand(Guid? UserId, Guid? CompanyId, CreateSubcategoryDto body) : IRequest<Response<SubcategoryDto>>;

}
