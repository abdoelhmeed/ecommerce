﻿using Ecommerce.Application.Dtos.SubcategoryDtos;

namespace Ecommerce.Application.Features.SubcategoryFeature.Commands.Create
{
    public class CreateSubcategoryCommandHandler : IRequestHandler<CreateSubcategoryCommand, Response<SubcategoryDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<CreateSubcategoryCommand> _logger;

        public CreateSubcategoryCommandHandler(AppDbContext appContext, IMapper mapper, ILogger<CreateSubcategoryCommand> logger)
        {
            _context = appContext;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<SubcategoryDto>> Handle(CreateSubcategoryCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var model = _mapper.Map<Subcategory>(request.body);
                _context.Subcategories.Add(model);
                var created = await _context.SaveChangesAsync();
                if (created > 0)
                    return new Response<SubcategoryDto>
                    {
                        Data = _mapper.Map<SubcategoryDto>(model),
                        Succeeded = true,
                        Message = " Created Successfully",
                        HttpStatusCode = HttpStatusCode.Created
                    };

                return new Response<SubcategoryDto>
                {
                    Succeeded = false,
                    Message = "Failed create agency",
                    HttpStatusCode = HttpStatusCode.BadRequest
                };

            }
            catch (Exception ex)
            {
                _logger.LogError("Error when create new agency" + ex.Message.ToString() + ex.InnerException?.Message.ToString());
                throw;
            }


        }
    }
}
