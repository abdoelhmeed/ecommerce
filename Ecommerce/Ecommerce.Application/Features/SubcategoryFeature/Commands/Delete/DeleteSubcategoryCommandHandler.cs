﻿

namespace Ecommerce.Application.Features.SubcategoryFeature.Commands.Delete
{
    public class DeleteSubcategoryCommandHandler : IRequestHandler<DeleteSubcategoryCommand, Response<Guid>>
    {
        private readonly AppDbContext _context;
        public readonly ILogger<DeleteSubcategoryCommandHandler> _Logger;

        public DeleteSubcategoryCommandHandler(AppDbContext context, ILogger<DeleteSubcategoryCommandHandler> logger)
        {
            _context = context;
            _Logger = logger;
        }

        public async Task<Response<Guid>> Handle(DeleteSubcategoryCommand request, CancellationToken cancellationToken)
        {
            var cat = await _context.Subcategories.FindAsync(request.Id) ?? throw new NotFoundException("Not Found");

            cat.IsDeleted = true;
            _context.Subcategories.Update(cat);
            var del = await _context.SaveChangesAsync();

            return new Response<Guid>
            {
                Data = request.Id,
                Message = "Deleted",
                HttpStatusCode = HttpStatusCode.OK,
                Succeeded = true
            };
        }
    }

}
