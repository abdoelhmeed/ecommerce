﻿

namespace Ecommerce.Application.Features.SubcategoryFeature.Commands.Delete
{
    public record DeleteSubcategoryCommand(Guid? UserId, Guid? CompanyId, Guid Id) : IRequest<Response<Guid>>;

}
