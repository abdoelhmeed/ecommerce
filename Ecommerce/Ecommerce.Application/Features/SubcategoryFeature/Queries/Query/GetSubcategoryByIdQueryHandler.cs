﻿using Ecommerce.Application.Dtos.SubcategoryDtos;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.Application.Features.SubcategoryFeature.Queries.Query
{
    public class GetSubcategoryByIdQueryHandler : IRequestHandler<GetSubcategoryByIdQuery, Response<SubcategoryDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetSubcategoryQueryHandler> _logger;

        public GetSubcategoryByIdQueryHandler(AppDbContext context, IMapper mapper, ILogger<GetSubcategoryQueryHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<SubcategoryDto>> Handle(GetSubcategoryByIdQuery request, CancellationToken cancellationToken)
        {
            var rel = await _context.Subcategories
                .Include(x => x.Category)
                .SingleOrDefaultAsync(x => x.Id == request.Id) ?? throw new NotFoundException("Not Found");

            return new Response<SubcategoryDto>
            {
                Data = _mapper.Map<SubcategoryDto>(rel),
                Succeeded = true,
                HttpStatusCode = HttpStatusCode.OK

            };

        }
    }

}
