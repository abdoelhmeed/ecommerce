﻿using Ecommerce.Application.Dtos.SubcategoryDtos;

namespace Ecommerce.Application.Features.SubcategoryFeature.Queries.Query
{
    public record GetSubcategoryByIdQuery(Guid? CompanyId, Guid Id) : IRequest<Response<SubcategoryDto>>;

}
