﻿using Ecommerce.Application.Dtos.SubcategoryDtos;

namespace Ecommerce.Application.Features.SubcategoryFeature.Queries.Query
{
    public record GetSubcategoryQuery(SubcategoryFilter filter) : IRequest<Response<PagedList<SubcategoryDto>>>;

}
