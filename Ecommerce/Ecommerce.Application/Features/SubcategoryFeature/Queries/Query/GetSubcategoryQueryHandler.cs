﻿using AutoMapper.QueryableExtensions;
using Ecommerce.Application.Dtos.SubcategoryDtos;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.Application.Features.SubcategoryFeature.Queries.Query
{
    public class GetSubcategoryQueryHandler : IRequestHandler<GetSubcategoryQuery, Response<PagedList<SubcategoryDto>>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetSubcategoryQueryHandler> _logger;

        public GetSubcategoryQueryHandler(AppDbContext context, IMapper mapper, ILogger<GetSubcategoryQueryHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<PagedList<SubcategoryDto>>> Handle(GetSubcategoryQuery request, CancellationToken cancellationToken)
        {
            IQueryable<Subcategory> query = _context.Subcategories
                .Include(x => x.Category)
                .Where(x => x.CompanyId == request.filter.CompanyId && x.IsDeleted == false);

            var result = query
                .OrderBy(o => o.CreateTime)
                .ProjectTo<SubcategoryDto>(_mapper.ConfigurationProvider);

            var data = await PagedList<SubcategoryDto>.ToPagedListAsync(result, request.filter.PageNumber, request.filter.PageSize);

            return new Response<PagedList<SubcategoryDto>>
            {
                Data = data,
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                Succeeded = true,
            };
        }
    }

}
