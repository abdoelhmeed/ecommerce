﻿using AutoMapper.QueryableExtensions;
using Ecommerce.Application.Dtos.CountryDtos;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.Application.Features.CountryFeature.Queries.Query
{
    public class GetCountryQueryHandler : IRequestHandler<GetCountryQuery, Response<List<CountryDto>>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetCountryQueryHandler> _logger;

        public GetCountryQueryHandler(AppDbContext context, IMapper mapper, ILogger<GetCountryQueryHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<List<CountryDto>>> Handle(GetCountryQuery request, CancellationToken cancellationToken)
        {
            IQueryable<Country> query = _context.Countries
                .Where(x => x.IsDeleted == false);

            var result = query
                .OrderBy(o => o.CreateTime)
                .ProjectTo<CountryDto>(_mapper.ConfigurationProvider);

            var data = await result.ToListAsync();

            return new Response<List<CountryDto>>
            {
                Data = data,
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                Succeeded = true,
            };
        }
    }

}
