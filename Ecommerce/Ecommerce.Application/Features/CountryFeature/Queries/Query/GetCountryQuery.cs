﻿using Ecommerce.Application.Dtos.CountryDtos;

namespace Ecommerce.Application.Features.CountryFeature.Queries.Query
{
    public record GetCountryQuery : IRequest<Response<List<CountryDto>>>;

}
