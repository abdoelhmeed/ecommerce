﻿using Ecommerce.Application.Dtos.CountryDtos;

namespace Ecommerce.Application.Features.CountryFeature.Queries.Query
{
    public class GetCountryByIdQueryHandler : IRequestHandler<GetCountryByIdQuery, Response<CountryDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetCountryQueryHandler> _logger;

        public GetCountryByIdQueryHandler(AppDbContext context, IMapper mapper, ILogger<GetCountryQueryHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<CountryDto>> Handle(GetCountryByIdQuery request, CancellationToken cancellationToken)
        {
            var rel = await _context.Countries.FindAsync(request.Id) ?? throw new NotFoundException("Not Found");

            return new Response<CountryDto>
            {
                Data = _mapper.Map<CountryDto>(rel),
                Succeeded = true,
                HttpStatusCode = HttpStatusCode.OK

            };

        }
    }

}
