﻿using Ecommerce.Application.Dtos.CountryDtos;

namespace Ecommerce.Application.Features.CountryFeature.Queries.Query
{
    public record GetCountryByIdQuery(Guid? CompanyId, Guid Id) : IRequest<Response<CountryDto>>;

}
