﻿

namespace Ecommerce.Application.Features.CountryFeature.Commands.Delete
{
    public class DeleteCountryCommandHandler : IRequestHandler<DeleteCountryCommand, Response<Guid>>
    {
        private readonly AppDbContext _context;
        public readonly ILogger<DeleteCountryCommandHandler> _Logger;

        public DeleteCountryCommandHandler(AppDbContext context, ILogger<DeleteCountryCommandHandler> logger)
        {
            _context = context;
            _Logger = logger;
        }

        public async Task<Response<Guid>> Handle(DeleteCountryCommand request, CancellationToken cancellationToken)
        {
            var cat = await _context.Countries.FindAsync(request.Id) ?? throw new NotFoundException("Not Found");

            cat.IsDeleted = true;
            _context.Countries.Update(cat);
            var del = await _context.SaveChangesAsync();

            return new Response<Guid>
            {
                Data = request.Id,
                Message = "Deleted",
                HttpStatusCode = HttpStatusCode.OK,
                Succeeded = true
            };
        }
    }

}
