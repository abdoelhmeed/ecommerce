﻿

namespace Ecommerce.Application.Features.CountryFeature.Commands.Delete
{
    public record DeleteCountryCommand(Guid? UserId, Guid? CompanyId, Guid Id) : IRequest<Response<Guid>>;

}
