﻿using Ecommerce.Application.Dtos.CountryDtos;

namespace Ecommerce.Application.Features.CountryFeature.Commands.Update
{
    public record UpdateCountryCommand(Guid? UserId, Guid? CompanyId, Guid Id, UpdateCountryDto body) : IRequest<Response<CountryDto>>;


}
