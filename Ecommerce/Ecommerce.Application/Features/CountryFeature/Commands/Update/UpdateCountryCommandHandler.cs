﻿using Ecommerce.Application.Dtos.CountryDtos;

namespace Ecommerce.Application.Features.CountryFeature.Commands.Update
{
    public class UpdateCountryCommandHandler : IRequestHandler<UpdateCountryCommand, Response<CountryDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdateCountryCommandHandler> _logger;

        public UpdateCountryCommandHandler(AppDbContext context, IMapper mapper, ILogger<UpdateCountryCommandHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<CountryDto>> Handle(UpdateCountryCommand request, CancellationToken cancellationToken)
        {
            Country country = await _context.Countries.FindAsync(request.Id) ?? throw new NotFoundException("Country Not Found");

            country.UpdateTime = DateTime.UtcNow;
            country.UpdateBy = request.UserId;
            country.NameAr = request.body.NameAr;
            country.NameEn = request.body.NameEn;


            _context.Countries.Update(country);

            var updated = await _context.SaveChangesAsync();

            if (updated > 0)
                return new Response<CountryDto>
                {
                    Data = _mapper.Map<CountryDto>(country),
                    HttpStatusCode = HttpStatusCode.OK,
                    Succeeded = true,
                };


            return new Response<CountryDto>
            {
                Data = _mapper.Map<CountryDto>(country),
                HttpStatusCode = HttpStatusCode.BadRequest,
                Succeeded = true,
            };


        }
    }
}
