﻿using Ecommerce.Application.Dtos.CountryDtos;

namespace Ecommerce.Application.Features.CountryFeature.Commands.Create
{
    public record CreateCountryCommand(Guid? UserId, Guid? CompanyId, CreateCountryDto body) : IRequest<Response<CountryDto>>;

}
