﻿using Ecommerce.Application.Dtos.CountryDtos;

namespace Ecommerce.Application.Features.CountryFeature.Commands.Create
{
    public class CreateCountryCommandHandler : IRequestHandler<CreateCountryCommand, Response<CountryDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<CreateCountryCommand> _logger;

        public CreateCountryCommandHandler(AppDbContext appContext, IMapper mapper, ILogger<CreateCountryCommand> logger)
        {
            _context = appContext;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<CountryDto>> Handle(CreateCountryCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var model = _mapper.Map<Country>(request.body);
                _context.Countries.Add(model);
                var created = await _context.SaveChangesAsync();
                if (created > 0)
                    return new Response<CountryDto>
                    {
                        Data = _mapper.Map<CountryDto>(model),
                        Succeeded = true,
                        Message = " Created Successfully",
                        HttpStatusCode = HttpStatusCode.Created
                    };

                return new Response<CountryDto>
                {
                    Succeeded = false,
                    Message = "Failed create agency",
                    HttpStatusCode = HttpStatusCode.BadRequest
                };

            }
            catch (Exception ex)
            {
                _logger.LogError("Error when create new agency" + ex.Message.ToString() + ex.InnerException?.Message.ToString());
                throw;
            }


        }
    }
}
