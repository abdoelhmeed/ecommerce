﻿using Ecommerce.Application.Dtos.BrandDtos;

namespace Ecommerce.Application.Features.BrandFeature.Queries.Query
{
    public record GetBrandByIdQuery(Guid? CompanyId, Guid Id) : IRequest<Response<BrandDto>>;

}
