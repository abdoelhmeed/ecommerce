﻿using Ecommerce.Application.Dtos.BrandDtos;

namespace Ecommerce.Application.Features.BrandFeature.Queries.Query
{
    public class GetBrandByIdQueryHandler : IRequestHandler<GetBrandByIdQuery, Response<BrandDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetBrandQueryHandler> _logger;

        public GetBrandByIdQueryHandler(AppDbContext context, IMapper mapper, ILogger<GetBrandQueryHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<BrandDto>> Handle(GetBrandByIdQuery request, CancellationToken cancellationToken)
        {
            var rel = await _context.Brands.FindAsync(request.Id) ?? throw new NotFoundException("Not Found");

            return new Response<BrandDto>
            {
                Data = _mapper.Map<BrandDto>(rel),
                Succeeded = true,
                HttpStatusCode = HttpStatusCode.OK

            };

        }
    }

}
