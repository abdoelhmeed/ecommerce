﻿using AutoMapper.QueryableExtensions;
using Ecommerce.Application.Dtos.BrandDtos;

namespace Ecommerce.Application.Features.BrandFeature.Queries.Query
{
    public class GetBrandQueryHandler : IRequestHandler<GetBrandQuery, Response<PagedList<BrandDto>>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetBrandQueryHandler> _logger;

        public GetBrandQueryHandler(AppDbContext context, IMapper mapper, ILogger<GetBrandQueryHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<PagedList<BrandDto>>> Handle(GetBrandQuery request, CancellationToken cancellationToken)
        {
            IQueryable<Brand> query = _context.Brands
                .Where(x => x.CompanyId == request.filter.CompanyId && x.IsDeleted == false);

            var result = query
                .OrderBy(o => o.CreateTime)
                .ProjectTo<BrandDto>(_mapper.ConfigurationProvider);

            var data = await PagedList<BrandDto>.ToPagedListAsync(result, request.filter.PageNumber, request.filter.PageSize);

            return new Response<PagedList<BrandDto>>
            {
                Data = data,
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                Succeeded = true,
            };
        }
    }

}
