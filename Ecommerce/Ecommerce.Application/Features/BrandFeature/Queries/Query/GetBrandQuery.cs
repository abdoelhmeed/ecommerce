﻿using Ecommerce.Application.Dtos.BrandDtos;

namespace Ecommerce.Application.Features.BrandFeature.Queries.Query
{
    public record GetBrandQuery(BrandFilter filter) : IRequest<Response<PagedList<BrandDto>>>;

}
