﻿

namespace Ecommerce.Application.Features.BrandFeature.Commands.Delete
{
    public class DeleteBrandCommandHandler : IRequestHandler<DeleteBrandCommand, Response<Guid>>
    {
        private readonly AppDbContext _context;
        public readonly ILogger<DeleteBrandCommandHandler> _Logger;

        public DeleteBrandCommandHandler(AppDbContext context, ILogger<DeleteBrandCommandHandler> logger)
        {
            _context = context;
            _Logger = logger;
        }

        public async Task<Response<Guid>> Handle(DeleteBrandCommand request, CancellationToken cancellationToken)
        {
            var cat = await _context.Brands.FindAsync(request.Id) ?? throw new NotFoundException("Not Found");

            cat.IsDeleted = true;
            _context.Brands.Update(cat);
            var del = await _context.SaveChangesAsync();

            return new Response<Guid>
            {
                Data = request.Id,
                Message = "Deleted",
                HttpStatusCode = HttpStatusCode.OK,
                Succeeded = true
            };
        }
    }

}
