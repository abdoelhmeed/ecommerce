﻿

namespace Ecommerce.Application.Features.BrandFeature.Commands.Delete
{
    public record DeleteBrandCommand(Guid? UserId, Guid? CompanyId, Guid Id) : IRequest<Response<Guid>>;

}
