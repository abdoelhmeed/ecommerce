﻿using Ecommerce.Application.Dtos.BrandDtos;

namespace Ecommerce.Application.Features.BrandFeature.Commands.Create
{
    public record CreateBrandCommand(Guid? UserId, Guid? CompanyId, CreateBrandDto body) : IRequest<Response<BrandDto>>;

}
