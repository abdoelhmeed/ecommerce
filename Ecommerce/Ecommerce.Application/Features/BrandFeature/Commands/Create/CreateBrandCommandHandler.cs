﻿using Ecommerce.Application.Dtos.BrandDtos;

namespace Ecommerce.Application.Features.BrandFeature.Commands.Create
{
    public class CreateBrandCommandHandler : IRequestHandler<CreateBrandCommand, Response<BrandDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<CreateBrandCommandHandler> _logger;

        public CreateBrandCommandHandler(AppDbContext appContext, IMapper mapper, ILogger<CreateBrandCommandHandler> logger)
        {
            _context = appContext;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<BrandDto>> Handle(CreateBrandCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var model = _mapper.Map<Brand>(request.body);
                _context.Brands.Add(model);
                var created = await _context.SaveChangesAsync();
                if (created > 0)
                    return new Response<BrandDto>
                    {
                        Data = _mapper.Map<BrandDto>(model),
                        Succeeded = true,
                        Message = " Created Successfully",
                        HttpStatusCode = HttpStatusCode.Created
                    };

                return new Response<BrandDto>
                {
                    Succeeded = false,
                    Message = "Failed create agency",
                    HttpStatusCode = HttpStatusCode.BadRequest
                };

            }
            catch (Exception ex)
            {
                _logger.LogError("Error when create new agency" + ex.Message.ToString() + ex.InnerException?.Message.ToString());
                throw;
            }


        }
    }
}
