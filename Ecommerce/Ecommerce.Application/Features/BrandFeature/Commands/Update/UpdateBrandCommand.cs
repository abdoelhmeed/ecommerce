﻿using Ecommerce.Application.Dtos.BrandDtos;

namespace Ecommerce.Application.Features.BrandFeature.Commands.Update
{
    public record UpdateBrandCommand(Guid? UserId, Guid? CompanyId, Guid Id, UpdateBrandDto body) : IRequest<Response<BrandDto>>;


}
