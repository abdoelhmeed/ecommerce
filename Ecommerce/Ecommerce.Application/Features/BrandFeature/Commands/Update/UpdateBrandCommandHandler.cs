﻿using Ecommerce.Application.Dtos.BrandDtos;

namespace Ecommerce.Application.Features.BrandFeature.Commands.Update
{
    public class UpdateBrandCommandHandler : IRequestHandler<UpdateBrandCommand, Response<BrandDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdateBrandCommandHandler> _logger;

        public UpdateBrandCommandHandler(AppDbContext context, IMapper mapper, ILogger<UpdateBrandCommandHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<BrandDto>> Handle(UpdateBrandCommand request, CancellationToken cancellationToken)
        {
            Brand Brand = await _context.Brands.FindAsync(request.Id) ?? throw new NotFoundException("Brand Not Found");

            Brand.UpdateTime = DateTime.UtcNow;
            Brand.UpdateBy = request.UserId;
            Brand.NameAr = request.body.NameAr;
            Brand.NameEn = request.body.NameEn;

            if (Brand.Image != null)
                Brand.Image = request.body.Image;


            _context.Brands.Update(Brand);

            var updated = await _context.SaveChangesAsync();

            if (updated > 0)
                return new Response<BrandDto>
                {
                    Data = _mapper.Map<BrandDto>(Brand),
                    HttpStatusCode = HttpStatusCode.OK,
                    Succeeded = true,
                };


            return new Response<BrandDto>
            {
                Data = _mapper.Map<BrandDto>(Brand),
                HttpStatusCode = HttpStatusCode.BadRequest,
                Succeeded = true,
            };


        }
    }
}
