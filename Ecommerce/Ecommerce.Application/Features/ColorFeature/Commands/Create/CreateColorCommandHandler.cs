﻿using Ecommerce.Application.Dtos.ColorDtos;

namespace Ecommerce.Application.Features.ColorFeature.Commands.Create
{
    public class CreateColorCommandHandler : IRequestHandler<CreateColorCommand, Response<ColorDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<CreateColorCommand> _logger;

        public CreateColorCommandHandler(AppDbContext appContext, IMapper mapper, ILogger<CreateColorCommand> logger)
        {
            _context = appContext;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<ColorDto>> Handle(CreateColorCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var model = _mapper.Map<Color>(request.body);
                _context.Colors.Add(model);
                var created = await _context.SaveChangesAsync();
                if (created > 0)
                    return new Response<ColorDto>
                    {
                        Data = _mapper.Map<ColorDto>(model),
                        Succeeded = true,
                        Message = " Created Successfully",
                        HttpStatusCode = HttpStatusCode.Created
                    };

                return new Response<ColorDto>
                {
                    Succeeded = false,
                    Message = "Failed create Color",
                    HttpStatusCode = HttpStatusCode.BadRequest
                };

            }
            catch (Exception ex)
            {
                _logger.LogError("Error when create new Color" + ex.Message.ToString() + ex.InnerException?.Message.ToString());
                throw;
            }


        }
    }
}
