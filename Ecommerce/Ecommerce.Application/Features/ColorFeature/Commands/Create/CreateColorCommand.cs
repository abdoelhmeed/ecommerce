﻿using Ecommerce.Application.Dtos.ColorDtos;

namespace Ecommerce.Application.Features.ColorFeature.Commands.Create
{
    public record CreateColorCommand(Guid? UserId, Guid? CompanyId, CreateColorDto body) : IRequest<Response<ColorDto>>;

}
