﻿

namespace Ecommerce.Application.Features.ColorFeature.Commands.Delete
{
    public record DeleteColorCommand(Guid? UserId, Guid? CompanyId, Guid Id) : IRequest<Response<Guid>>;

}
