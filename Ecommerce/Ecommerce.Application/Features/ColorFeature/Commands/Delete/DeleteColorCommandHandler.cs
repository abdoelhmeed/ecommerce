﻿

namespace Ecommerce.Application.Features.ColorFeature.Commands.Delete
{
    public class DeleteColorCommandHandler : IRequestHandler<DeleteColorCommand, Response<Guid>>
    {
        private readonly AppDbContext _context;
        public readonly ILogger<DeleteColorCommandHandler> _Logger;

        public DeleteColorCommandHandler(AppDbContext context, ILogger<DeleteColorCommandHandler> logger)
        {
            _context = context;
            _Logger = logger;
        }

        public async Task<Response<Guid>> Handle(DeleteColorCommand request, CancellationToken cancellationToken)
        {
            var cat = await _context.Colors.FindAsync(request.Id) ?? throw new NotFoundException("Not Found");

            cat.IsDeleted = true;
            _context.Colors.Update(cat);
            var del = await _context.SaveChangesAsync();

            return new Response<Guid>
            {
                Data = request.Id,
                Message = "Deleted",
                HttpStatusCode = HttpStatusCode.OK,
                Succeeded = true
            };
        }
    }

}
