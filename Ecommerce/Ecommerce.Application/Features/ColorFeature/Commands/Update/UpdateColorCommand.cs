﻿using Ecommerce.Application.Dtos.ColorDtos;

namespace Ecommerce.Application.Features.ColorFeature.Commands.Update
{
    public record UpdateColorCommand(Guid? UserId, Guid? CompanyId, Guid Id, UpdateColorDto body) : IRequest<Response<ColorDto>>;


}
