﻿using Ecommerce.Application.Dtos.ColorDtos;

namespace Ecommerce.Application.Features.ColorFeature.Commands.Update
{
    public class UpdateColorCommandHandler : IRequestHandler<UpdateColorCommand, Response<ColorDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdateColorCommandHandler> _logger;

        public UpdateColorCommandHandler(AppDbContext context, IMapper mapper, ILogger<UpdateColorCommandHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<ColorDto>> Handle(UpdateColorCommand request, CancellationToken cancellationToken)
        {
            Color Color = await _context.Colors.FindAsync(request.Id) ?? throw new NotFoundException("Color Not Found");

            Color.UpdateTime = DateTime.UtcNow;
            Color.UpdateBy = request.UserId;
            Color.Name = request.body.Name;
            if (Color.Image != null)
                Color.Image = request.body.Image;


            _context.Colors.Update(Color);

            var updated = await _context.SaveChangesAsync();

            if (updated > 0)
                return new Response<ColorDto>
                {
                    Data = _mapper.Map<ColorDto>(Color),
                    HttpStatusCode = HttpStatusCode.OK,
                    Succeeded = true,
                };


            return new Response<ColorDto>
            {
                Data = _mapper.Map<ColorDto>(Color),
                HttpStatusCode = HttpStatusCode.BadRequest,
                Succeeded = true,
            };


        }
    }
}
