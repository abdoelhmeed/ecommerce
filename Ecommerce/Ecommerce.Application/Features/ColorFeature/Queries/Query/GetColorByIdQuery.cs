﻿using Ecommerce.Application.Dtos.ColorDtos;

namespace Ecommerce.Application.Features.ColorFeature.Queries.Query
{
    public record GetColorByIdQuery(Guid? CompanyId, Guid Id) : IRequest<Response<ColorDto>>;

}
