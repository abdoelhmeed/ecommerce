﻿using Ecommerce.Application.Dtos.ColorDtos;

namespace Ecommerce.Application.Features.ColorFeature.Queries.Query
{
    public class GetColorByIdQueryHandler : IRequestHandler<GetColorByIdQuery, Response<ColorDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetColorQueryHandler> _logger;

        public GetColorByIdQueryHandler(AppDbContext context, IMapper mapper, ILogger<GetColorQueryHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<ColorDto>> Handle(GetColorByIdQuery request, CancellationToken cancellationToken)
        {
            var rel = await _context.Colors.FindAsync(request.Id) ?? throw new NotFoundException("Not Found");

            return new Response<ColorDto>
            {
                Data = _mapper.Map<ColorDto>(rel),
                Succeeded = true,
                HttpStatusCode = HttpStatusCode.OK

            };

        }
    }

}
