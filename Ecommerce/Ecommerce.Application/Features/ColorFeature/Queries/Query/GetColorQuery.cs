﻿using Ecommerce.Application.Dtos.ColorDtos;

namespace Ecommerce.Application.Features.ColorFeature.Queries.Query
{
    public record GetColorQuery(ColorFilter filter) : IRequest<Response<PagedList<ColorDto>>>;

}
