﻿using AutoMapper.QueryableExtensions;
using Ecommerce.Application.Dtos.ColorDtos;

namespace Ecommerce.Application.Features.ColorFeature.Queries.Query
{
    public class GetColorQueryHandler : IRequestHandler<GetColorQuery, Response<PagedList<ColorDto>>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetColorQueryHandler> _logger;

        public GetColorQueryHandler(AppDbContext context, IMapper mapper, ILogger<GetColorQueryHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<PagedList<ColorDto>>> Handle(GetColorQuery request, CancellationToken cancellationToken)
        {
            IQueryable<Color> query = _context.Colors
                .Where(x => x.CompanyId == request.filter.CompanyId && x.IsDeleted == false);

            var result = query
                .OrderBy(o => o.CreateTime)
                .ProjectTo<ColorDto>(_mapper.ConfigurationProvider);

            var data = await PagedList<ColorDto>.ToPagedListAsync(result, request.filter.PageNumber, request.filter.PageSize);

            return new Response<PagedList<ColorDto>>
            {
                Data = data,
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                Succeeded = true,
            };
        }
    }

}
