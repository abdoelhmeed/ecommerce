﻿using Ecommerce.Application.Dtos.ClothingSizeDtos;

namespace Ecommerce.Application.Features.ClothingSizeFeature.Commands.Update
{
    public class UpdateClothingSizeCommandHandler : IRequestHandler<UpdateClothingSizeCommand, Response<ClothingSizeDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdateClothingSizeCommandHandler> _logger;

        public UpdateClothingSizeCommandHandler(AppDbContext context, IMapper mapper, ILogger<UpdateClothingSizeCommandHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<ClothingSizeDto>> Handle(UpdateClothingSizeCommand request, CancellationToken cancellationToken)
        {
            ClothingSize ClothingSize = await _context.ClothingSizes.FindAsync(request.Id) ?? throw new NotFoundException("ClothingSize Not Found");

            ClothingSize.UpdateTime = DateTime.UtcNow;
            ClothingSize.UpdateBy = request.UserId;
            ClothingSize.Name = request.body.Name;



            _context.ClothingSizes.Update(ClothingSize);

            var updated = await _context.SaveChangesAsync();

            if (updated > 0)
                return new Response<ClothingSizeDto>
                {
                    Data = _mapper.Map<ClothingSizeDto>(ClothingSize),
                    HttpStatusCode = HttpStatusCode.OK,
                    Succeeded = true,
                };


            return new Response<ClothingSizeDto>
            {
                Data = _mapper.Map<ClothingSizeDto>(ClothingSize),
                HttpStatusCode = HttpStatusCode.BadRequest,
                Succeeded = true,
            };


        }
    }
}
