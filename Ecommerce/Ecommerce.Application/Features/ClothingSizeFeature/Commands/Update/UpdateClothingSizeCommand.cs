﻿using Ecommerce.Application.Dtos.ClothingSizeDtos;

namespace Ecommerce.Application.Features.ClothingSizeFeature.Commands.Update
{
    public record UpdateClothingSizeCommand(Guid? UserId, Guid? CompanyId, Guid Id, UpdateClothingSizeDto body) : IRequest<Response<ClothingSizeDto>>;


}
