﻿using Ecommerce.Application.Dtos.ClothingSizeDtos;

namespace Ecommerce.Application.Features.ClothingSizeFeature.Commands.Create
{
    public record CreateClothingSizeCommand(Guid? UserId, Guid? CompanyId, CreateClothingSizeDto body) : IRequest<Response<ClothingSizeDto>>;

}
