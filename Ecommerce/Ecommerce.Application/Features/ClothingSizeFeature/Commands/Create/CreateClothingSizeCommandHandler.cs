﻿using Ecommerce.Application.Dtos.ClothingSizeDtos;

namespace Ecommerce.Application.Features.ClothingSizeFeature.Commands.Create
{
    public class CreateClothingSizeCommandHandler : IRequestHandler<CreateClothingSizeCommand, Response<ClothingSizeDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<CreateClothingSizeCommand> _logger;

        public CreateClothingSizeCommandHandler(AppDbContext appContext, IMapper mapper, ILogger<CreateClothingSizeCommand> logger)
        {
            _context = appContext;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<ClothingSizeDto>> Handle(CreateClothingSizeCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var model = _mapper.Map<ClothingSize>(request.body);
                _context.ClothingSizes.Add(model);
                var created = await _context.SaveChangesAsync();
                if (created > 0)
                    return new Response<ClothingSizeDto>
                    {
                        Data = _mapper.Map<ClothingSizeDto>(model),
                        Succeeded = true,
                        Message = " Created Successfully",
                        HttpStatusCode = HttpStatusCode.Created
                    };

                return new Response<ClothingSizeDto>
                {
                    Succeeded = false,
                    Message = "Failed create agency",
                    HttpStatusCode = HttpStatusCode.BadRequest
                };

            }
            catch (Exception ex)
            {
                _logger.LogError("Error when create new agency" + ex.Message.ToString() + ex.InnerException?.Message.ToString());
                throw;
            }


        }
    }
}
