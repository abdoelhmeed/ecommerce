﻿

namespace Ecommerce.Application.Features.ClothingSizeFeature.Commands.Delete
{
    public class DeleteClothingSizeCommandHandler : IRequestHandler<DeleteClothingSizeCommand, Response<Guid>>
    {
        private readonly AppDbContext _context;
        public readonly ILogger<DeleteClothingSizeCommandHandler> _Logger;

        public DeleteClothingSizeCommandHandler(AppDbContext context, ILogger<DeleteClothingSizeCommandHandler> logger)
        {
            _context = context;
            _Logger = logger;
        }

        public async Task<Response<Guid>> Handle(DeleteClothingSizeCommand request, CancellationToken cancellationToken)
        {
            var cat = await _context.ClothingSizes.FindAsync(request.Id) ?? throw new NotFoundException("Not Found");

            cat.IsDeleted = true;
            _context.ClothingSizes.Update(cat);
            var del = await _context.SaveChangesAsync();

            return new Response<Guid>
            {
                Data = request.Id,
                Message = "Deleted",
                HttpStatusCode = HttpStatusCode.OK,
                Succeeded = true
            };
        }
    }

}
