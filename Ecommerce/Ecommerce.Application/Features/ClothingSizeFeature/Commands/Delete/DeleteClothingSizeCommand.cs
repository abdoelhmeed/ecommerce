﻿

namespace Ecommerce.Application.Features.ClothingSizeFeature.Commands.Delete
{
    public record DeleteClothingSizeCommand(Guid? UserId, Guid? CompanyId, Guid Id) : IRequest<Response<Guid>>;

}
