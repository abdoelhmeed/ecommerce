﻿using Ecommerce.Application.Dtos.ClothingSizeDtos;

namespace Ecommerce.Application.Features.ClothingSizeFeature.Queries.Query
{
    public record GetClothingSizeByIdQuery(Guid? CompanyId, Guid Id) : IRequest<Response<ClothingSizeDto>>;

}
