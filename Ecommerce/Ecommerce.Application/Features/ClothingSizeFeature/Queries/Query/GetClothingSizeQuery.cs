﻿using Ecommerce.Application.Dtos.ClothingSizeDtos;

namespace Ecommerce.Application.Features.ClothingSizeFeature.Queries.Query
{
    public record GetClothingSizeQuery(ClothingSizeFilterDto filter) : IRequest<Response<PagedList<ClothingSizeDto>>>;

}
