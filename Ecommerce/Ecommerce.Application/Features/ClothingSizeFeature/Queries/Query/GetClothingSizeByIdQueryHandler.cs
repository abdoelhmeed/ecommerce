﻿using Ecommerce.Application.Dtos.ClothingSizeDtos;
using Ecommerce.Shared.Exceptions;

namespace Ecommerce.Application.Features.ClothingSizeFeature.Queries.Query
{
    public class GetClothingSizeByIdQueryHandler : IRequestHandler<GetClothingSizeByIdQuery, Response<ClothingSizeDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetClothingSizeQueryHandler> _logger;

        public GetClothingSizeByIdQueryHandler(AppDbContext context, IMapper mapper, ILogger<GetClothingSizeQueryHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<ClothingSizeDto>> Handle(GetClothingSizeByIdQuery request, CancellationToken cancellationToken)
        {
            var rel = await _context.ClothingSizes.FindAsync(request.Id) ?? throw new NotFoundException("Not Found");

            return new Response<ClothingSizeDto>
            {
                Data = _mapper.Map<ClothingSizeDto>(rel),
                Succeeded = true,
                HttpStatusCode = HttpStatusCode.OK

            };

        }
    }

}
