﻿using AutoMapper.QueryableExtensions;
using Ecommerce.Application.Dtos.ClothingSizeDtos;

namespace Ecommerce.Application.Features.ClothingSizeFeature.Queries.Query
{
    public class GetClothingSizeQueryHandler : IRequestHandler<GetClothingSizeQuery, Response<PagedList<ClothingSizeDto>>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetClothingSizeQueryHandler> _logger;

        public GetClothingSizeQueryHandler(AppDbContext context, IMapper mapper, ILogger<GetClothingSizeQueryHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<PagedList<ClothingSizeDto>>> Handle(GetClothingSizeQuery request, CancellationToken cancellationToken)
        {
            IQueryable<ClothingSize> query = _context.ClothingSizes
                .Where(x => x.CompanyId == request.filter.CompanyId && x.IsDeleted == false);

            var result = query
                .OrderBy(o => o.CreateTime)
                .ProjectTo<ClothingSizeDto>(_mapper.ConfigurationProvider);

            var data = await PagedList<ClothingSizeDto>.ToPagedListAsync(result, request.filter.PageNumber, request.filter.PageSize);

            return new Response<PagedList<ClothingSizeDto>>
            {
                Data = data,
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                Succeeded = true,
            };
        }
    }

}
