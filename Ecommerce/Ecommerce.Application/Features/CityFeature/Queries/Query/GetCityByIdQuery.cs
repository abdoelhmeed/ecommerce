﻿using Ecommerce.Application.Dtos.CityDtos;

namespace Ecommerce.Application.Features.CityFeature.Queries.Query
{
    public record GetCityByIdQuery(Guid? CompanyId, Guid Id) : IRequest<Response<CityDto>>;

}
