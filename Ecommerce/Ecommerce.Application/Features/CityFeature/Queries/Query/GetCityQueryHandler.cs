﻿using AutoMapper.QueryableExtensions;
using Ecommerce.Application.Dtos.CityDtos;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.Application.Features.CityFeature.Queries.Query
{
    public class GetCityQueryHandler : IRequestHandler<GetCityQuery, Response<List<CityDto>>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetCityQueryHandler> _logger;

        public GetCityQueryHandler(AppDbContext context, IMapper mapper, ILogger<GetCityQueryHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<List<CityDto>>> Handle(GetCityQuery request, CancellationToken cancellationToken)
        {
            IQueryable<City> query = _context.Cities
                .Include(x => x.Country)
                .Where(x => x.IsDeleted == false);

            if (request.filter.CountryId == null)
                query = query.Where(x => x.CountryId == request.filter.CountryId);

            var result = query
                .OrderBy(o => o.CreateTime)
                .ProjectTo<CityDto>(_mapper.ConfigurationProvider);

            List<CityDto> cities = await result.ToListAsync();


            return new Response<List<CityDto>>
            {
                Data = cities,
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                Succeeded = true,
            };
        }
    }

}
