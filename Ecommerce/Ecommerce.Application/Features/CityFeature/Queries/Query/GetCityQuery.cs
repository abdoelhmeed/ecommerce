﻿using Ecommerce.Application.Dtos.CityDtos;

namespace Ecommerce.Application.Features.CityFeature.Queries.Query
{
    public record GetCityQuery(CityFilter filter) : IRequest<Response<List<CityDto>>>;

}
