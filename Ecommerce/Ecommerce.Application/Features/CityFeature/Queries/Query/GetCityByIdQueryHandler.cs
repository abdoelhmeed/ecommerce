﻿using Ecommerce.Application.Dtos.CityDtos;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.Application.Features.CityFeature.Queries.Query
{
    public class GetCityByIdQueryHandler : IRequestHandler<GetCityByIdQuery, Response<CityDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetCityQueryHandler> _logger;

        public GetCityByIdQueryHandler(AppDbContext context, IMapper mapper, ILogger<GetCityQueryHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<CityDto>> Handle(GetCityByIdQuery request, CancellationToken cancellationToken)
        {
            var rel = await _context.Cities.Include(x => x.Country).SingleOrDefaultAsync(x => x.Id == request.Id) ?? throw new NotFoundException("Not Found");

            return new Response<CityDto>
            {
                Data = _mapper.Map<CityDto>(rel),
                Succeeded = true,
                HttpStatusCode = HttpStatusCode.OK

            };

        }
    }

}
