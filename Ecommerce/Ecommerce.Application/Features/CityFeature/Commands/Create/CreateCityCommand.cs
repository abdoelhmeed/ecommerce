﻿using Ecommerce.Application.Dtos.CityDtos;

namespace Ecommerce.Application.Features.CityFeature.Commands.Create
{
    public record CreateCityCommand(Guid? UserId, Guid? CompanyId, CreateCityDto body) : IRequest<Response<CityDto>>;

}
