﻿using Ecommerce.Application.Dtos.CityDtos;

namespace Ecommerce.Application.Features.CityFeature.Commands.Create
{
    public class CreateCityCommandHandler : IRequestHandler<CreateCityCommand, Response<CityDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<CreateCityCommand> _logger;

        public CreateCityCommandHandler(AppDbContext appContext, IMapper mapper, ILogger<CreateCityCommand> logger)
        {
            _context = appContext;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<CityDto>> Handle(CreateCityCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var model = _mapper.Map<City>(request.body);
                _context.Cities.Add(model);
                var created = await _context.SaveChangesAsync();
                if (created > 0)
                    return new Response<CityDto>
                    {
                        Data = _mapper.Map<CityDto>(model),
                        Succeeded = true,
                        Message = " Created Successfully",
                        HttpStatusCode = HttpStatusCode.Created
                    };

                return new Response<CityDto>
                {
                    Succeeded = false,
                    Message = "Failed create agency",
                    HttpStatusCode = HttpStatusCode.BadRequest
                };

            }
            catch (Exception ex)
            {
                _logger.LogError("Error when create new agency" + ex.Message.ToString() + ex.InnerException?.Message.ToString());
                throw;
            }


        }
    }
}
