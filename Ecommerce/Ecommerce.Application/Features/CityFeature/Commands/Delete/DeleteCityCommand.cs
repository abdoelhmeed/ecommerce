﻿

namespace Ecommerce.Application.Features.CityFeature.Commands.Delete
{
    public record DeleteCityCommand(Guid? UserId, Guid? CompanyId, Guid Id) : IRequest<Response<Guid>>;

}
