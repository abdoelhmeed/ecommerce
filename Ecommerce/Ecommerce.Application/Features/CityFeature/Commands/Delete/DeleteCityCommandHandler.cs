﻿

namespace Ecommerce.Application.Features.CityFeature.Commands.Delete
{
    public class DeleteCityCommandHandler : IRequestHandler<DeleteCityCommand, Response<Guid>>
    {
        private readonly AppDbContext _context;
        public readonly ILogger<DeleteCityCommandHandler> _Logger;

        public DeleteCityCommandHandler(AppDbContext context, ILogger<DeleteCityCommandHandler> logger)
        {
            _context = context;
            _Logger = logger;
        }

        public async Task<Response<Guid>> Handle(DeleteCityCommand request, CancellationToken cancellationToken)
        {
            var cat = await _context.Cities.FindAsync(request.Id) ?? throw new NotFoundException("Not Found");

            cat.IsDeleted = true;
            _context.Cities.Update(cat);
            var del = await _context.SaveChangesAsync();

            return new Response<Guid>
            {
                Data = request.Id,
                Message = "Deleted",
                HttpStatusCode = HttpStatusCode.OK,
                Succeeded = true
            };
        }
    }

}
