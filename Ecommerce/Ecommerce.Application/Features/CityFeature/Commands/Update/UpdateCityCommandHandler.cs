﻿using Ecommerce.Application.Dtos.CityDtos;

namespace Ecommerce.Application.Features.CityFeature.Commands.Update
{
    public class UpdateCityCommandHandler : IRequestHandler<UpdateCityCommand, Response<CityDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdateCityCommandHandler> _logger;

        public UpdateCityCommandHandler(AppDbContext context, IMapper mapper, ILogger<UpdateCityCommandHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<CityDto>> Handle(UpdateCityCommand request, CancellationToken cancellationToken)
        {
            City City = await _context.Cities.FindAsync(request.Id) ?? throw new NotFoundException("City Not Found");

            City.UpdateTime = DateTime.UtcNow;
            City.UpdateBy = request.UserId;
            City.NameAr = request.body.NameAr;
            City.NameEn = request.body.NameEn;
            City.CountryId = request.body.CountryId;



            _context.Cities.Update(City);

            var updated = await _context.SaveChangesAsync();

            if (updated > 0)
                return new Response<CityDto>
                {
                    Data = _mapper.Map<CityDto>(City),
                    HttpStatusCode = HttpStatusCode.OK,
                    Succeeded = true,
                };


            return new Response<CityDto>
            {
                Data = _mapper.Map<CityDto>(City),
                HttpStatusCode = HttpStatusCode.BadRequest,
                Succeeded = true,
            };


        }
    }
}
