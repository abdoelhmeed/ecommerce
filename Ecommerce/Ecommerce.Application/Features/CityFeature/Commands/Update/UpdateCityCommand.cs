﻿using Ecommerce.Application.Dtos.CityDtos;

namespace Ecommerce.Application.Features.CityFeature.Commands.Update
{
    public record UpdateCityCommand(Guid? UserId, Guid? CompanyId, Guid Id, UpdateCityDto body) : IRequest<Response<CityDto>>;


}
