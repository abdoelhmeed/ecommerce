﻿using Ecommerce.Application.Dtos.CatgoryDtos;

namespace Ecommerce.Application.Features.CategoryFeature.Queries.Query
{
    public record GetCatgoryQuery(CatgoryFilter filter) : IRequest<Response<PagedList<CatgoryDto>>>;

}
