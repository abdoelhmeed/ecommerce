﻿using Ecommerce.Application.Dtos.CatgoryDtos;
using Ecommerce.Shared.Exceptions;

namespace Ecommerce.Application.Features.CategoryFeature.Queries.Query
{
    public class GetCatgoryByIdQueryHandler : IRequestHandler<GetCatgoryByIdQuery, Response<CatgoryDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetCatgoryQueryHandler> _logger;

        public GetCatgoryByIdQueryHandler(AppDbContext context, IMapper mapper, ILogger<GetCatgoryQueryHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<CatgoryDto>> Handle(GetCatgoryByIdQuery request, CancellationToken cancellationToken)
        {
            var rel = await _context.Categories.FindAsync(request.Id) ?? throw new NotFoundException("Not Found");

            return new Response<CatgoryDto>
            {
                Data = _mapper.Map<CatgoryDto>(rel),
                Succeeded = true,
                HttpStatusCode = HttpStatusCode.OK

            };

        }
    }

}
