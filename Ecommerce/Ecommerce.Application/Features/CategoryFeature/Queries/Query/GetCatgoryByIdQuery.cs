﻿using Ecommerce.Application.Dtos.CatgoryDtos;

namespace Ecommerce.Application.Features.CategoryFeature.Queries.Query
{
    public record GetCatgoryByIdQuery(Guid? CompanyId, Guid Id) : IRequest<Response<CatgoryDto>>;

}
