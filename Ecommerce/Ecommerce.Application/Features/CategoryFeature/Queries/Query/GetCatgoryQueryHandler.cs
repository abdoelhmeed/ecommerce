﻿using AutoMapper.QueryableExtensions;
using Ecommerce.Application.Dtos.CatgoryDtos;

namespace Ecommerce.Application.Features.CategoryFeature.Queries.Query
{
    public class GetCatgoryQueryHandler : IRequestHandler<GetCatgoryQuery, Response<PagedList<CatgoryDto>>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<GetCatgoryQueryHandler> _logger;

        public GetCatgoryQueryHandler(AppDbContext context, IMapper mapper, ILogger<GetCatgoryQueryHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<PagedList<CatgoryDto>>> Handle(GetCatgoryQuery request, CancellationToken cancellationToken)
        {
            IQueryable<Category> query = _context.Categories
                .Where(x => x.CompanyId == request.filter.CompanyId && x.IsDeleted == false);

            var result = query
                .OrderBy(o => o.CreateTime)
                .ProjectTo<CatgoryDto>(_mapper.ConfigurationProvider);

            var data = await PagedList<CatgoryDto>.ToPagedListAsync(result, request.filter.PageNumber, request.filter.PageSize);

            return new Response<PagedList<CatgoryDto>>
            {
                Data = data,
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                Succeeded = true,
            };
        }
    }

}
