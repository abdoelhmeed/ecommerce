﻿using Ecommerce.Application.Dtos.CatgoryDtos;
using Ecommerce.Shared.Exceptions;

namespace Ecommerce.Application.Features.CategoryFeature.Commands.Update
{
    public class UpdateCatgoryCommandHandler : IRequestHandler<UpdateCatgoryCommand, Response<CatgoryDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdateCatgoryCommandHandler> _logger;

        public UpdateCatgoryCommandHandler(AppDbContext context, IMapper mapper, ILogger<UpdateCatgoryCommandHandler> logger)
        {
            _context = context;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<CatgoryDto>> Handle(UpdateCatgoryCommand request, CancellationToken cancellationToken)
        {
            Category category = await _context.Categories.FindAsync(request.Id) ?? throw new NotFoundException("Category Not Found");

            category.UpdateTime = DateTime.UtcNow;
            category.UpdateBy = request.UserId;
            category.NameAr = request.body.NameAr;
            category.NameEn = request.body.NameEn;
            category.DescriptionEn = request.body.DescriptionEn;
            category.DescriptionAr = request.body.DescriptionAr;
            if (category.CImage != null)
                category.CImage = request.body.CImage;


            _context.Categories.Update(category);

            var updated = await _context.SaveChangesAsync();

            if (updated > 0)
                return new Response<CatgoryDto>
                {
                    Data = _mapper.Map<CatgoryDto>(category),
                    HttpStatusCode = HttpStatusCode.OK,
                    Succeeded = true,
                };


            return new Response<CatgoryDto>
            {
                Data = _mapper.Map<CatgoryDto>(category),
                HttpStatusCode = HttpStatusCode.BadRequest,
                Succeeded = true,
            };


        }
    }
}
