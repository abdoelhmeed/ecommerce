﻿using Ecommerce.Application.Dtos.CatgoryDtos;

namespace Ecommerce.Application.Features.CategoryFeature.Commands.Update
{
    public record UpdateCatgoryCommand(Guid? UserId, Guid? CompanyId, Guid Id, UpdateCatgoryDto body) : IRequest<Response<CatgoryDto>>;


}
