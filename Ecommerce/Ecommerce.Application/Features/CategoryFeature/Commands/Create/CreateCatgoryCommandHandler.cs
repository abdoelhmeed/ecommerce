﻿using Ecommerce.Application.Dtos.CatgoryDtos;
using System.Net;

namespace Ecommerce.Application.Features.CategoryFeature.Commands.Create
{
    public class CreateCatgoryCommandHandler : IRequestHandler<CreateCatgoryCommand, Response<CatgoryDto>>
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ILogger<CreateCatgoryCommand> _logger;

        public CreateCatgoryCommandHandler(AppDbContext appContext, IMapper mapper, ILogger<CreateCatgoryCommand> logger)
        {
            _context = appContext;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<Response<CatgoryDto>> Handle(CreateCatgoryCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var model = _mapper.Map<Category>(request.body);
                _context.Categories.Add(model);
                var created = await _context.SaveChangesAsync();
                if (created > 0)
                    return new Response<CatgoryDto>
                    {
                        Data = _mapper.Map<CatgoryDto>(model),
                        Succeeded = true,
                        Message = " Created Successfully",
                        HttpStatusCode = HttpStatusCode.Created
                    };

                return new Response<CatgoryDto>
                {
                    Succeeded = false,
                    Message = "Failed create agency",
                    HttpStatusCode = HttpStatusCode.BadRequest
                };

            }
            catch (Exception ex)
            {
                _logger.LogError("Error when create new agency" + ex.Message.ToString() + ex.InnerException?.Message.ToString());
                throw;
            }


        }
    }
}
