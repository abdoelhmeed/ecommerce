﻿using Ecommerce.Application.Dtos.CatgoryDtos;

namespace Ecommerce.Application.Features.CategoryFeature.Commands.Create
{
    public record CreateCatgoryCommand(Guid? UserId, Guid? CompanyId, CreateCatgoryDto body) : IRequest<Response<CatgoryDto>>;

}
