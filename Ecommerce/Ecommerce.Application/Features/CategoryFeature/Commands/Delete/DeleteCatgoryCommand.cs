﻿

namespace Ecommerce.Application.Features.CategoryFeature.Commands.Delete
{
    public record DeleteCatgoryCommand(Guid? UserId, Guid? CompanyId, Guid Id) : IRequest<Response<Guid>>;

}
