﻿

namespace Ecommerce.Application.Features.CategoryFeature.Commands.Delete
{
    public class DeleteCatgoryCommandHandler : IRequestHandler<DeleteCatgoryCommand, Response<Guid>>
    {
        private readonly AppDbContext _context;
        public readonly ILogger<DeleteCatgoryCommandHandler> _Logger;

        public DeleteCatgoryCommandHandler(AppDbContext context, ILogger<DeleteCatgoryCommandHandler> logger)
        {
            _context = context;
            _Logger = logger;
        }

        public async Task<Response<Guid>> Handle(DeleteCatgoryCommand request, CancellationToken cancellationToken)
        {
            var cat = await _context.Categories.FindAsync(request.Id) ?? throw new NotFoundException("Not Found");

            cat.IsDeleted = true;
            _context.Categories.Update(cat);
            var del = await _context.SaveChangesAsync();

            return new Response<Guid>
            {
                Data = request.Id,
                Message = "Deleted",
                HttpStatusCode = HttpStatusCode.OK,
                Succeeded = true
            };
        }
    }

}
