﻿namespace Ecommerce.Application.Dtos.CountryDtos
{
    public class UpdateCountryDto
    {
        public Guid Id { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
    }
}
