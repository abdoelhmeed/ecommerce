﻿namespace Ecommerce.Application.Dtos.CountryDtos
{
    public class CreateCountryDto
    {
        public string NameAr { get; set; }
        public string NameEn { get; set; }
    }
}
