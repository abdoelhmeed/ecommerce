﻿namespace Ecommerce.Application.Dtos.BrandDtos
{
    public class CreateBrandDto
    {
        public string NameEn { get; set; } // الاسم باللغة الإنجليزية
        public string NameAr { get; set; } // الاسم باللغة العربية
        public string Image { get; set; }
    }
}
