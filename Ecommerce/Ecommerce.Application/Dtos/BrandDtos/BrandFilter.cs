﻿namespace Ecommerce.Application.Dtos.BrandDtos
{
    public class BrandFilter : PaginationParamsDto
    {
        public Guid? CompanyId { get; set; }
    }
}
