﻿using System.ComponentModel.DataAnnotations;

namespace Ecommerce.Application.Dtos.BrandDtos
{
    public class BrandDto
    {
        public Guid Id { get; set; }
        [Required]
        public string NameEn { get; set; } // الاسم باللغة الإنجليزية
        public string NameAr { get; set; } // الاسم باللغة العربية
        public string Image { get; set; }
    }
}
