﻿namespace Ecommerce.Application.Dtos.BrandDtos
{
    public class UpdateBrandDto
    {
        public Guid Id { get; set; }
        public string NameEn { get; set; } // الاسم باللغة الإنجليزية
        public string NameAr { get; set; } // الاسم باللغة العربية
        public string Image { get; set; }
    }
}
