﻿namespace Ecommerce.Application.Dtos.SubcategoryDtos
{
    public class CreateSubcategoryDto
    {
        public string NameEn { get; set; } // الاسم باللغة الإنجليزية
        public string NameAr { get; set; } // الاسم باللغة العربية
        public string DescriptionEn { get; set; } // وصف المنتج
        public string DescriptionAr { get; set; } // وصف المنتج
        public Guid? CategoryId { get; set; }
        public string Image { get; set; }
        public Guid? CompanyId { get; set; }
    }
}
