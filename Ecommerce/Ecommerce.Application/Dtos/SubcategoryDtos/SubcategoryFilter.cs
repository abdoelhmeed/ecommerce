﻿namespace Ecommerce.Application.Dtos.SubcategoryDtos
{
    public class SubcategoryFilter : PaginationParamsDto
    {
        public string Value { get; set; }
        public Guid? CompanyId { get; set; }
        public Guid? CategoryId { get; set; }
    }
}
