﻿namespace Ecommerce.Application.Dtos.CityDtos
{
    public class CityFilter : PaginationParamsDto
    {
        public Guid? CountryId { get; set; }
    }
}
