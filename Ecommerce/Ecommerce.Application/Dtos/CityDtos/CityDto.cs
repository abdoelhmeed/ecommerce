﻿namespace Ecommerce.Application.Dtos.CityDtos
{
    public class CityDto
    {
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public Guid? CountryId { get; set; }
        public string CountryNameAr { get; set; }
        public string CountryNameEn { get; set; }
    }
}
