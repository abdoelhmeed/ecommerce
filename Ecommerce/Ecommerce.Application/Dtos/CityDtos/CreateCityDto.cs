﻿namespace Ecommerce.Application.Dtos.CityDtos
{
    public class CreateCityDto
    {
        public Guid Id { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public Guid? CountryId { get; set; }

    }
}
