﻿namespace Ecommerce.Application.Dtos.ColorDtos
{
    public class ColorFilter : PaginationParamsDto
    {
        public Guid? CompanyId { get; set; }
    }
}
