﻿namespace Ecommerce.Application.Dtos.ColorDtos
{
    public class UpdateColorDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string HexValue { get; set; }
        public string Image { get; set; }
    }
}
