﻿namespace Ecommerce.Application.Dtos.CatgoryDtos
{
    public class CatgoryFilter : PaginationParamsDto
    {
        public string Value { get; set; }
        public Guid? CompanyId { get; set; }
    }
}
