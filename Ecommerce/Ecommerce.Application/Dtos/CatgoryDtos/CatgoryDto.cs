﻿namespace Ecommerce.Application.Dtos.CatgoryDtos
{
    public class CatgoryDto
    {
        public Guid Id { get; set; }
        public string NameEn { get; set; } // الاسم باللغة الإنجليزية
        public string NameAr { get; set; } // الاسم باللغة العربية
        public string DescriptionEn { get; set; } // وصف المنتج
        public string DescriptionAr { get; set; } // وصف المنتج
        public string CImage { get; set; }
    }
}
