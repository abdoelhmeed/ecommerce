﻿namespace Ecommerce.Application.Dtos.ClothingSizeDtos
{
    public class UpdateClothingSizeDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
