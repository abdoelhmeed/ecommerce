﻿namespace Ecommerce.Application.Dtos.ClothingSizeDtos
{
    public class CreateClothingSizeDto
    {
        public string Name { get; set; }
    }
}
