﻿namespace Ecommerce.Application.Dtos.ClothingSizeDtos
{
    public class ClothingSizeFilterDto : PaginationParamsDto
    {
        public Guid? CompanyId { get; set; }
    }
}
