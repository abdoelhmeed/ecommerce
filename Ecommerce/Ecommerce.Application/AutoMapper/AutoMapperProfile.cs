﻿using Ecommerce.Application.Dtos.CatgoryDtos;

namespace Ecommerce.Application.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<CreateCatgoryDto, Category>().ReverseMap();
            CreateMap<CatgoryDto, Category>().ReverseMap();
        }
    }
}
