﻿using AutoMapper;
using Ecommerce.Application.AutoMapper;
using Ecommerce.Application.Dtos.BrandDtos;
using Ecommerce.Application.Features.BrandFeature.Commands.Create;
using Ecommerce.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;

namespace Ecommerce.Application.Tests.Features.BrandFeature.Commands.Create
{
    public class CreateBrandCommandHandlerTests : IDisposable
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly Mock<ILogger<CreateBrandCommandHandler>> _mockLogger;
        private readonly CreateBrandCommandHandler _handler;

        public CreateBrandCommandHandlerTests()
        {
            // Setup In-Memory Database
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseSqlServer("Data Source=DESKTOP-SAKSD8Q\\SQLEXPRESS;Initial Catalog=EEcommerce-DB;Integrated Security=True;Persist Security Info=False;Pooling=False;Multiple Active Result Sets=False;Encrypt=True;Trust Server Certificate=True;Command Timeout=0") // Unique DB for each test
                .Options;

            _context = new AppDbContext(options);
            _context.Database.EnsureCreated();

            // Setup AutoMapper
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AutoMapperProfile>(); // Ensure your mapping profiles are added
            });
            _mapper = mapperConfig.CreateMapper();

            // Setup Mock Logger
            _mockLogger = new Mock<ILogger<CreateBrandCommandHandler>>();

            // Initialize Handler
            _handler = new CreateBrandCommandHandler(_context, _mapper, _mockLogger.Object);
        }

        [Fact]
        public async Task Handle_Should_Create_Brand_Successfully()
        {
            // Arrange
            var createBrandDto = new CreateBrandDto
            {
                NameEn = "fff",
                NameAr = "ffff",
                Image = "fff"
                // Add other necessary properties
            };

            var command = new CreateBrandCommand
            (Guid.NewGuid(),
                Guid.NewGuid(),
                createBrandDto);


            // Act
            var result = await _handler.Handle(command, CancellationToken.None);

            // Assert
            Assert.NotNull(result);


        }


        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}
