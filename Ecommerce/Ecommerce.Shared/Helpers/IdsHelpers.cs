﻿namespace Ecommerce.Shared.Helpers
{
    public class IdsHelpers
    {
        // استخدام أحرف كبيرة في اسم الحقل نفسه 
        public static string ADMINISTRATOR_ID = "B22698B8-42A2-4115-9631-1C2D1E2AC5F7";

        // ولأنها private (قد تختار إما إبقاءها private أو تعديلها حسب الحاجة)
        public static string ROLE_ADMIN_ID = "2301D884-221A-4E7D-B509-0113DCC043E1";
        public static string ROLE_CLIENT_ID = "2301D884-221A-4E7D-B509-0113DCC043E3";
        public static string ROLE_EMPLOYEE_ID = "7D9B7113-A8F8-4035-99A7-A20DD411F6A3";

    }
}
