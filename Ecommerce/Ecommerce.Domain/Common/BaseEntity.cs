﻿namespace Ecommerce.Domain.Common
{
    public class BaseEntity<T>
    {
        public T Id { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime CreateTime { get; set; } = DateTime.Now;
        public Guid? CreateBy { get; set; }
        public DateTime UpdateTime { get; set; } = DateTime.Now;
        public Guid? UpdateBy { get; set; }
    }
}
