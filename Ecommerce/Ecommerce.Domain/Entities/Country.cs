﻿using Ecommerce.Domain.Common;

namespace Ecommerce.Domain.Entities
{
    public class Country : BaseEntity<Guid>
    {
        public string NameAr { get; set; }

        public string NameEn { get; set; }

        // علاقة One-to-Many مع City
        public virtual ICollection<City> Cities { get; set; }
    }


}
