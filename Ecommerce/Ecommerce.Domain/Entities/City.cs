﻿using Ecommerce.Domain.Common;

namespace Ecommerce.Domain.Entities
{
    public class City : BaseEntity<Guid>
    {


        public string NameAr { get; set; }

        public string NameEn { get; set; }


        public Guid? CountryId { get; set; }
        public virtual Country Country { get; set; }


        public virtual ICollection<Company> Companies { get; set; }

    }

}
