﻿using Ecommerce.Domain.Common;

namespace Ecommerce.Domain.Entities
{
    public class Product : BaseEntity<Guid>
    {
        public string NameEn { get; set; } // الاسم باللغة الإنجليزية
        public string NameAr { get; set; } // الاسم باللغة العربية
        public string DescriptionEn { get; set; } // وصف المنتج
        public string DescriptionAr { get; set; } // وصف المنتج
        public decimal Price { get; set; } // السعر الحالي
        public decimal OriginalPrice { get; set; } // السعر الأصلي
        public decimal DiscountPercentage { get; set; } // نسبة الخصم
        public string SKU { get; set; } // رقم تعريف المنتج
        public string Tags { get; set; } // الوسوم
        public int Ratings { get; set; } // عدد التقييمات
        public Guid? SubcategoryId { get; set; }
        public Subcategory Subcategory { get; set; } // العلاقة مع الفئة الرئيسية
        public Guid? BrandId { get; set; }
        public Brand Brand { get; set; } // العلاقة مع الفئة الرئيسية
        public virtual ICollection<ProductProperty> ProductProperties { get; set; }
        public virtual ICollection<ProductVariant> ProductVariants { get; set; }

        public virtual Guid? CompanyId { get; set; }
        public virtual Company Company { get; set; }

    }


}
