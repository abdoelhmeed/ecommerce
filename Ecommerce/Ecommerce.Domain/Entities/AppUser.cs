﻿using Microsoft.AspNetCore.Identity;

namespace Ecommerce.Domain.Entities
{
    public class AppUser : IdentityUser
    {
        public string FullName { get; set; }
        public virtual Guid? CompanyId { get; set; }
        public virtual Company Company { get; set; }
    }

}
