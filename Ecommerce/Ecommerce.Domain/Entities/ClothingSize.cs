﻿using Ecommerce.Domain.Common;

namespace Ecommerce.Domain.Entities
{
    public class ClothingSize : BaseEntity<Guid>
    {
        public string Name { get; set; }
        public virtual ICollection<ProductVariant> ProductVariants { get; set; }
        public virtual Guid? CompanyId { get; set; }
        public virtual Company Company { get; set; }
    }


}
