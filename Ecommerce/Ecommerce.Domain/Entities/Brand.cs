﻿using Ecommerce.Domain.Common;

namespace Ecommerce.Domain.Entities
{
    public class Brand : BaseEntity<Guid>
    {
        public string NameEn { get; set; } // الاسم باللغة الإنجليزية
        public string NameAr { get; set; } // الاسم باللغة العربية
        public string Image { get; set; }
        public ICollection<Product> Products { get; set; }

        public virtual Guid? CompanyId { get; set; }
        public virtual Company Company { get; set; }
    }


}
