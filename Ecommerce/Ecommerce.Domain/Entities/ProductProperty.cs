﻿using Ecommerce.Domain.Common;
using System.ComponentModel.DataAnnotations;

namespace Ecommerce.Domain.Entities
{
    public class ProductProperty : BaseEntity<Guid>
    {

        // المفتاح الأجنبي لجدول المنتجات
        public Guid? ProductId { get; set; }
        public virtual Product Product { get; set; }

        // اسم الخاصية (مثل: "FabricType", "ScreenSize", "StrapMaterial", إلخ)
        [Required, MaxLength(100)]
        public string PropertyName { get; set; }

        // القيمة المرتبطة بالخاصية (مثل: "Cotton", "6.5 inches", "Leather")
        [MaxLength(500)]
        public string PropertyValue { get; set; }

        // يمكنك إضافة حقول أخرى حسب الحاجة (مثل نوع البيانات، ترتيب العرض، إلخ)


        public virtual Guid? CompanyId { get; set; }
        public virtual Company Company { get; set; }
    }


}
