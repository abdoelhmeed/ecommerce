﻿using Ecommerce.Domain.Common;
using System.ComponentModel.DataAnnotations;

namespace Ecommerce.Domain.Entities
{
    public class Company : BaseEntity<Guid>
    {
        [Required]
        [MaxLength(225)]
        public string NameEn { get; set; } // الاسم باللغة الإنجليزية

        [Required]
        [MaxLength(225)]
        public string NameAr { get; set; } // الاسم باللغة العربية

        [MaxLength(500)]
        public string Address { get; set; }

        [EmailAddress]
        [MaxLength(255)]
        public string Email { get; set; }

        [MaxLength(50)]
        public string Phone { get; set; }

        public bool IsActive { get; set; } = true;

        public string Logo { get; set; }

        // تاريخ نهاية الاشتراك
        public DateTime? SubscriptionEndDate { get; set; }

        // الرقم الضريبي
        [MaxLength(50)]
        public string VatNumber { get; set; }

        // السجل التجاري
        [MaxLength(50)]
        public string CommercialRegistrationNumber { get; set; }


        public virtual Guid? CityId { get; set; }
        public virtual City City { get; set; }

        public virtual ICollection<Brand> Brands { get; set; }
        public virtual ICollection<Category> Categories { get; set; }
        public virtual ICollection<ClothingSize> ClothingSizes { get; set; }
        public virtual ICollection<Subcategory> Subcategories { get; set; }
        public virtual ICollection<Color> Colors { get; set; }
        public virtual ICollection<Product> Products { get; set; }
        public virtual ICollection<ProductProperty> ProductProperties { get; set; }
        public virtual ICollection<ProductVariant> ProductVariants { get; set; }
        public virtual ICollection<AppUser> AppUsers { get; set; }

    }
}
