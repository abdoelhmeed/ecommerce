﻿using Ecommerce.Domain.Common;

namespace Ecommerce.Domain.Entities
{
    public class Subcategory : BaseEntity<Guid>
    {
        public string NameEn { get; set; } // الاسم باللغة الإنجليزية
        public string NameAr { get; set; } // الاسم باللغة العربية
        public string DescriptionEn { get; set; } // وصف المنتج
        public string DescriptionAr { get; set; } // وصف المنتج

        // معرف الفئة الرئيسية (Foreign Key)
        public Guid? CategoryId { get; set; }
        public Category Category { get; set; } // العلاقة مع الفئة الرئيسية
        public string Image { get; set; }
        public virtual ICollection<Product> Products { get; set; }

        public virtual Guid? CompanyId { get; set; }
        public virtual Company Company { get; set; }
    }

}
