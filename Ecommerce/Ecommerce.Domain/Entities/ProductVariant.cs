﻿using Ecommerce.Domain.Common;

namespace Ecommerce.Domain.Entities
{
    public class ProductVariant : BaseEntity<Guid>
    {
        // المفتاح الأجنبي إلى Product
        public Guid? ProductId { get; set; }
        public virtual Product Product { get; set; }

        // المفتاح الأجنبي إلى Color
        public Guid? ColorId { get; set; }
        public virtual Color Color { get; set; }

        // إذا كنت تستخدم enum للمقاسات
        public Guid? ClothingSizeId { get; set; }
        public ClothingSize ClothingSize { get; set; }

        // الكمية المتوفرة لكل مزيج (Color + Size)
        public int StockQuantity { get; set; }

        public virtual Guid? CompanyId { get; set; }
        public virtual Company Company { get; set; }
    }


}
