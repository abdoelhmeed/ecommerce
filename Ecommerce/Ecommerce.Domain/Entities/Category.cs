﻿using Ecommerce.Domain.Common;

namespace Ecommerce.Domain.Entities
{
    public class Category : BaseEntity<Guid>
    {
        public string NameEn { get; set; } // الاسم باللغة الإنجليزية
        public string NameAr { get; set; } // الاسم باللغة العربية
        public string DescriptionEn { get; set; } // وصف المنتج
        public string DescriptionAr { get; set; } // وصف المنتج
        public string CImage { get; set; }
        public virtual ICollection<Subcategory> Subcategories { get; set; }

        public virtual Guid? CompanyId { get; set; }
        public virtual Company Company { get; set; }

    }

}
