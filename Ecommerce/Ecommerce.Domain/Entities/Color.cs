﻿using Ecommerce.Domain.Common;
using System.ComponentModel.DataAnnotations;

namespace Ecommerce.Domain.Entities
{
    public class Color : BaseEntity<Guid>
    {
        public string Name { get; set; }

        // يمكن إضافة حقول إضافية مثل الرمز اللوني (Hex)
        [MaxLength(7)]
        public string HexValue { get; set; }
        public string Image { get; set; }
        public virtual ICollection<ProductVariant> ProductVariants { get; set; }
        public virtual Guid? CompanyId { get; set; }
        public virtual Company Company { get; set; }

    }


}
