﻿using Ecommerce.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ecommerce.Domain.Configuration
{
    public class ProductVariantConfiguration : IEntityTypeConfiguration<ProductVariant>
    {
        public void Configure(EntityTypeBuilder<ProductVariant> entity)
        {
            entity.ToTable("ProductVariant");
            entity.HasKey(e => e.Id);

            entity.Property(e => e.Id)
                  .HasDefaultValueSql("newsequentialid()")
                  .ValueGeneratedOnAdd();


            entity.HasOne(b => b.Product)
                  .WithMany(p => p.ProductVariants)
                  .HasForeignKey(p => p.ProductId)
                  .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(b => b.Color)
                  .WithMany(p => p.ProductVariants)
                  .HasForeignKey(p => p.ColorId)
                  .OnDelete(DeleteBehavior.SetNull);



            entity.HasOne(b => b.ClothingSize)
                  .WithMany(p => p.ProductVariants)
                  .HasForeignKey(p => p.ClothingSizeId)
                  .OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(b => b.Company)
                  .WithMany(p => p.ProductVariants)
                  .HasForeignKey(p => p.CompanyId)
                  .OnDelete(DeleteBehavior.SetNull);
        }
    }
}

