﻿using Ecommerce.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ecommerce.Domain.Configuration
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> entity)
        {
            // اسم الجدول في قاعدة البيانات
            entity.ToTable("Product");

            // المفتاح الأساسي
            entity.HasKey(e => e.Id);

            // تعيين قيمة GUID افتراضية (newsequentialid) إذا كنت تستخدم SQL Server
            entity.Property(e => e.Id)
                  .HasDefaultValueSql("newsequentialid()")
                  .ValueGeneratedOnAdd();

            // ---- ضبط الحقول النصية ----
            // اسم المنتج باللغة الإنجليزية
            entity.Property(e => e.NameEn)
                  .HasMaxLength(200);

            // اسم المنتج باللغة العربية
            entity.Property(e => e.NameAr)
                  .HasMaxLength(200);

            // وصف المنتج باللغة الإنجليزية
            entity.Property(e => e.DescriptionEn)
            .HasMaxLength(500);

            entity.Property(p => p.DiscountPercentage)
             .HasColumnType("decimal(18, 2)");

            entity.Property(p => p.OriginalPrice)
                .HasColumnType("decimal(18, 2)");


            // وصف المنتج باللغة العربية
            entity.Property(e => e.DescriptionAr)
                  .HasMaxLength(500);

            // يمكن ضبط بقية الحقول (Price, SKU, Tags, إلخ) إذا أردت تحديد طول أو نوع معين
            entity.Property(e => e.SKU).HasMaxLength(50);
            entity.Property(e => e.Tags).HasMaxLength(300);
            // مثلاً:
            entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");

            entity.HasOne(p => p.Company)
                  .WithMany(c => c.Products)   // تأكد أن لديك c.Products في كيان Company
                  .HasForeignKey(p => p.CompanyId)
                  .OnDelete(DeleteBehavior.SetNull);


        }
    }
}
