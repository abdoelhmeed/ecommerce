﻿using Ecommerce.Shared.Helpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ecommerce.Domain.Configuration
{
    public class RoleConfiguration : IEntityTypeConfiguration<IdentityRole>
    {

        public void Configure(EntityTypeBuilder<IdentityRole> builder)
        {
            builder.HasData(
                new IdentityRole { Id = IdsHelpers.ROLE_ADMIN_ID, Name = "Admin".ToLower(), NormalizedName = "Admin".ToUpper() },
                new IdentityRole { Id = IdsHelpers.ROLE_CLIENT_ID, Name = "CLIENT".ToLower(), NormalizedName = "CLIENT".ToUpper() },
                new IdentityRole { Id = IdsHelpers.ROLE_EMPLOYEE_ID, Name = "EMPLOYEE".ToLower(), NormalizedName = "EMPLOYEE".ToUpper() }
                );
        }
    }
}
