﻿using Ecommerce.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ecommerce.Domain.Configuration
{
    public class CompanyConfiguration : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> entity)
        {
            entity.ToTable("Company");
            entity.HasKey(e => e.Id);

            entity.Property(e => e.Id)
                  .HasDefaultValueSql("newsequentialid()")
                  .ValueGeneratedOnAdd();

            entity.Property(e => e.NameAr).HasMaxLength(200);
            entity.Property(e => e.Logo).HasColumnType("nText");
            entity.Property(e => e.Address).HasMaxLength(7);


            entity.HasMany(b => b.Brands)
                  .WithOne(p => p.Company)
                  .HasForeignKey(p => p.CompanyId);

            entity.HasMany(b => b.AppUsers)
                 .WithOne(p => p.Company)
                 .HasForeignKey(p => p.CompanyId);


            entity.HasMany(b => b.Colors)
                 .WithOne(p => p.Company)
                 .HasForeignKey(p => p.CompanyId)
                 .OnDelete(DeleteBehavior.SetNull);

            entity.HasMany(b => b.ClothingSizes)
                 .WithOne(p => p.Company)
                 .HasForeignKey(p => p.CompanyId);


            entity.HasMany(b => b.Products)
           .WithOne(p => p.Company)
           .HasForeignKey(p => p.CompanyId);


            entity.HasMany(b => b.ProductProperties)
            .WithOne(p => p.Company)
            .HasForeignKey(p => p.CompanyId);

            entity.HasMany(b => b.Categories)
         .WithOne(p => p.Company)
         .HasForeignKey(p => p.CompanyId);

            entity.HasMany(b => b.Subcategories)
             .WithOne(p => p.Company)
             .HasForeignKey(p => p.CompanyId);

            entity.HasOne(b => b.City)
              .WithMany(p => p.Companies)
              .HasForeignKey(p => p.CityId);
        }
    }
}

