﻿using Ecommerce.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ecommerce.Domain.Configuration
{
    public class ProductPropertyConfiguration : IEntityTypeConfiguration<ProductProperty>
    {
        public void Configure(EntityTypeBuilder<ProductProperty> entity)
        {
            entity.ToTable("ProductProperty");
            entity.HasKey(e => e.Id);

            entity.Property(e => e.Id)
                  .HasDefaultValueSql("newsequentialid()")
                  .ValueGeneratedOnAdd();


            entity.Property(e => e.PropertyName).HasMaxLength(200);
            entity.Property(e => e.PropertyValue).HasMaxLength(500);


            entity.HasOne(b => b.Product)
                  .WithMany(p => p.ProductProperties)
                  .HasForeignKey(p => p.ProductId)
                  .OnDelete(DeleteBehavior.SetNull);


            entity.HasOne(b => b.Company)
                  .WithMany(p => p.ProductProperties)
                  .HasForeignKey(p => p.CompanyId)
                  .OnDelete(DeleteBehavior.SetNull);
        }
    }
}

