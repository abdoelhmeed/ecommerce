﻿using Ecommerce.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ecommerce.Domain.Configuration
{
    public class ColorConfiguration : IEntityTypeConfiguration<Color>
    {
        public void Configure(EntityTypeBuilder<Color> entity)
        {
            entity.ToTable("Color");
            entity.HasKey(e => e.Id);

            entity.Property(e => e.Id)
                  .HasDefaultValueSql("newsequentialid()")
                  .ValueGeneratedOnAdd();

            entity.Property(e => e.Name).HasMaxLength(200);
            entity.Property(e => e.Image).HasColumnType("nText");
            entity.Property(e => e.HexValue).HasMaxLength(7);


            entity.HasOne(b => b.Company)
                  .WithMany(p => p.Colors)
                  .HasForeignKey(p => p.CompanyId)
                  .OnDelete(DeleteBehavior.SetNull);
        }
    }
}

