﻿using Ecommerce.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ecommerce.Domain.Configuration
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> entity)
        {
            entity.ToTable("Category");
            entity.HasKey(e => e.Id);

            entity.Property(e => e.Id)
                  .HasDefaultValueSql("newsequentialid()")
                  .ValueGeneratedOnAdd();

            entity.Property(e => e.NameEn).HasMaxLength(200);
            entity.Property(e => e.NameAr).HasMaxLength(200);
            entity.Property(e => e.DescriptionAr).HasMaxLength(500);
            entity.Property(e => e.DescriptionEn).HasMaxLength(500);
            entity.Property(e => e.CImage).HasColumnType("nvarchar(Max)");
            entity.HasMany(x => x.Subcategories)
                .WithOne(x => x.Category)
                .HasForeignKey(f => f.CategoryId);

        }



    }
}
