﻿using Ecommerce.Domain.Entities;
using Ecommerce.Shared.Helpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ecommerce.Domain.Configuration
{
    public class AppUserConfiguration : IEntityTypeConfiguration<AppUser>
    {
        public void Configure(EntityTypeBuilder<AppUser> entity)
        {
            entity.HasOne(b => b.Company)
                  .WithMany(p => p.AppUsers)
                  .HasForeignKey(p => p.CompanyId)
                  .OnDelete(DeleteBehavior.SetNull);

            var admin = new AppUser
            {
                Id = IdsHelpers.ADMINISTRATOR_ID,
                UserName = "Admin",
                FullName = "abdelhameed ishag",
                NormalizedUserName = "Admin".ToUpper(),
                Email = "abdoelhmeedsudan@gmail.com",
                NormalizedEmail = "abdoelhmeedsudan@gmail.com".ToUpper(),
                EmailConfirmed = true,
            };
            admin.PasswordHash = PassGenerate(admin);

            entity.HasData(admin);

        }


        public string PassGenerate(AppUser user)
        {
            var passHash = new PasswordHasher<AppUser>();
            return passHash.HashPassword(user, "Admin@0489");
        }
    }

}
