﻿using Ecommerce.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ecommerce.Domain.Configuration
{
    public class ClothingSizeConfiguration : IEntityTypeConfiguration<ClothingSize>
    {
        public void Configure(EntityTypeBuilder<ClothingSize> entity)
        {
            entity.ToTable("ClothingSize");
            entity.HasKey(e => e.Id);

            entity.Property(e => e.Id)
                  .HasDefaultValueSql("newsequentialid()")
                  .ValueGeneratedOnAdd();

            entity.Property(e => e.Name).HasMaxLength(225);


            entity.HasOne(b => b.Company)
                  .WithMany(p => p.ClothingSizes)
                  .HasForeignKey(p => p.CompanyId)
                  .OnDelete(DeleteBehavior.SetNull);
        }
    }
}

