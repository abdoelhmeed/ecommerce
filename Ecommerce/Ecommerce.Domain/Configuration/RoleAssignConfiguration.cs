﻿using Ecommerce.Shared.Helpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ecommerce.Domain.Configuration
{
    public class RoleAssignConfiguration : IEntityTypeConfiguration<IdentityUserRole<string>>
    {
        public void Configure(EntityTypeBuilder<IdentityUserRole<string>> builder)
        {
            IdentityUserRole<string> iur = new IdentityUserRole<string>
            {
                RoleId = IdsHelpers.ROLE_ADMIN_ID,
                UserId = IdsHelpers.ADMINISTRATOR_ID
            };

            builder.HasData(iur);
        }
    }

}
