﻿using Ecommerce.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ecommerce.Domain.Configuration
{
    public class SubcategoryConfiguration : IEntityTypeConfiguration<Subcategory>
    {
        public void Configure(EntityTypeBuilder<Subcategory> entity)
        {
            entity.ToTable("Subcategory");
            entity.HasKey(e => e.Id);

            entity.Property(e => e.Id)
                  .HasDefaultValueSql("newsequentialid()")
                  .ValueGeneratedOnAdd();

            entity.Property(e => e.NameEn).HasMaxLength(200);
            entity.Property(e => e.NameAr).HasMaxLength(200);
            entity.Property(e => e.DescriptionAr).HasMaxLength(500);
            entity.Property(e => e.DescriptionEn).HasMaxLength(500);

            entity.HasOne(x => x.Category)
                .WithMany(x => x.Subcategories)
                .HasForeignKey(f => f.CategoryId);




            entity.HasOne(x => x.Company)
                .WithMany(x => x.Subcategories)
                .HasForeignKey(f => f.CompanyId);

        }



    }
}
