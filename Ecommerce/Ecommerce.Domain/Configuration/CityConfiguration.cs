﻿using Ecommerce.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ecommerce.Domain.Configuration
{
    public class CityConfiguration : IEntityTypeConfiguration<City>
    {
        public void Configure(EntityTypeBuilder<City> entity)
        {
            entity.ToTable("City");
            entity.HasKey(e => e.Id);

            entity.Property(e => e.Id)
                  .HasDefaultValueSql("newsequentialid()")
                  .ValueGeneratedOnAdd();

            entity.Property(e => e.NameEn).HasMaxLength(225);
            entity.Property(e => e.NameAr).HasMaxLength(225);

            entity.HasOne(city => city.Country)
            .WithMany(country => country.Cities)
            .HasForeignKey(city => city.CountryId);

            entity.HasMany(b => b.Companies)
                  .WithOne(p => p.City)
                  .HasForeignKey(p => p.CityId)
                  .OnDelete(DeleteBehavior.SetNull);
        }
    }
}

