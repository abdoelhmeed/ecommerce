﻿using Ecommerce.Application.Dtos.CountryDtos;
using Ecommerce.Application.Features.CountryFeature.Commands.Create;
using Ecommerce.Application.Features.CountryFeature.Commands.Delete;
using Ecommerce.Application.Features.CountryFeature.Commands.Update;
using Ecommerce.Application.Features.CountryFeature.Queries.Query;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Ecommerce.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryController : BaseApiController<CountryController>
    {
        // GET: api/<CountryController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return await APIExecute(new GetCountryQuery());
        }

        // GET api/<CountryController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            return await APIExecute(new GetCountryByIdQuery(CompanyId, id));
        }

        // POST api/<CountryController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateCountryDto model)
        {
            return await APIExecute(new CreateCountryCommand(CurrentUserId, CompanyId, model));
        }

        // PUT api/<CountryController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] UpdateCountryDto model)
        {
            return await APIExecute(new UpdateCountryCommand(CurrentUserId, CompanyId, id, model));

        }

        // DELETE api/<CountryController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            return await APIExecute(new DeleteCountryCommand(CurrentUserId, CompanyId, id));

        }
    }
}
