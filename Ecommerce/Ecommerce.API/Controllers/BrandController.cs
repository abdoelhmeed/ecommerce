﻿using Ecommerce.Application.Dtos.BrandDtos;
using Ecommerce.Application.Features.BrandFeature.Commands.Create;
using Ecommerce.Application.Features.BrandFeature.Commands.Delete;
using Ecommerce.Application.Features.BrandFeature.Commands.Update;
using Ecommerce.Application.Features.BrandFeature.Queries.Query;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Ecommerce.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BrandController : BaseApiController<BrandController>
    {
        // GET: api/<BrandController>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] BrandFilter filter)
        {
            return await APIExecute(new GetBrandQuery(filter));
        }

        // GET api/<BrandController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            return await APIExecute(new GetBrandByIdQuery(CompanyId, id));
        }

        // POST api/<BrandController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateBrandDto model)
        {
            return await APIExecute(new CreateBrandCommand(CurrentUserId, CompanyId, model));
        }

        // PUT api/<BrandController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] UpdateBrandDto model)
        {
            return await APIExecute(new UpdateBrandCommand(CurrentUserId, CompanyId, id, model));

        }

        // DELETE api/<BrandController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            return await APIExecute(new DeleteBrandCommand(CurrentUserId, CompanyId, id));

        }
    }
}
