﻿using Ecommerce.Application.Dtos.ClothingSizeDtos;
using Ecommerce.Application.Features.ClothingSizeFeature.Commands.Create;
using Ecommerce.Application.Features.ClothingSizeFeature.Commands.Delete;
using Ecommerce.Application.Features.ClothingSizeFeature.Commands.Update;
using Ecommerce.Application.Features.ClothingSizeFeature.Queries.Query;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Ecommerce.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClothingSizeController : BaseApiController<ClothingSizeController>
    {
        // GET: api/<ClothingSizeController>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] ClothingSizeFilterDto filter)
        {
            return await APIExecute(new GetClothingSizeQuery(filter));
        }

        // GET api/<ClothingSizeController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            return await APIExecute(new GetClothingSizeByIdQuery(CompanyId, id));
        }

        // POST api/<ClothingSizeController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateClothingSizeDto model)
        {
            return await APIExecute(new CreateClothingSizeCommand(CurrentUserId, CompanyId, model));
        }

        // PUT api/<ClothingSizeController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] UpdateClothingSizeDto model)
        {
            return await APIExecute(new UpdateClothingSizeCommand(CurrentUserId, CompanyId, id, model));

        }

        // DELETE api/<ClothingSizeController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            return await APIExecute(new DeleteClothingSizeCommand(CurrentUserId, CompanyId, id));

        }
    }
}
