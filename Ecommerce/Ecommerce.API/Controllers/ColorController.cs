﻿using Ecommerce.Application.Dtos.ColorDtos;
using Ecommerce.Application.Features.ColorFeature.Commands.Create;
using Ecommerce.Application.Features.ColorFeature.Commands.Delete;
using Ecommerce.Application.Features.ColorFeature.Commands.Update;
using Ecommerce.Application.Features.ColorFeature.Queries.Query;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Ecommerce.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ColorController : BaseApiController<ColorController>
    {
        // GET: api/<ColorController>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] ColorFilter filter)
        {
            return await APIExecute(new GetColorQuery(filter));
        }

        // GET api/<ColorController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            return await APIExecute(new GetColorByIdQuery(CompanyId, id));
        }

        // POST api/<ColorController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateColorDto model)
        {
            return await APIExecute(new CreateColorCommand(CurrentUserId, CompanyId, model));
        }

        // PUT api/<ColorController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] UpdateColorDto model)
        {
            return await APIExecute(new UpdateColorCommand(CurrentUserId, CompanyId, id, model));

        }

        // DELETE api/<ColorController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            return await APIExecute(new DeleteColorCommand(CurrentUserId, CompanyId, id));

        }
    }
}
