﻿using Ecommerce.Shared.Exceptions;
using Ecommerce.Shared.Helpers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Ecommerce.API.Controllers
{
    /// <summary>
    /// Base class for API Controllers, providing Mediator, CurrentUserId, and a unified APIExecute method.
    /// </summary>
    public abstract class BaseApiController<T> : Controller where T : BaseApiController<T>
    {
        private IMediator _mediator;

        /// <summary>
        /// Lazily retrieves the Mediator instance from HttpContext.RequestServices.
        /// </summary>
        protected IMediator Mediator
            => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();

        /// <summary>
        /// Retrieves the current user's ID from the Claims (using 'uid' claim or a suitable claim).
        /// If not found, returns Guid.Empty.
        /// </summary>
        public Guid CurrentUserId
        {
            get
            {
                var userIdClaim = User.FindFirst("uid"); // أو استخدم ClaimTypes.NameIdentifier أو أي Claim حسب مشروعك
                if (userIdClaim != null && Guid.TryParse(userIdClaim.Value, out Guid userId))
                {
                    return userId;
                }
                return Guid.Empty;
            }
        }


        public Guid? CompanyId
        {
            get
            {
                var companyIdClaim = User.FindFirst("cid"); // Replace "cid" with your actual claim type for Company ID
                if (companyIdClaim != null && Guid.TryParse(companyIdClaim.Value, out Guid companyId))
                {
                    return companyId;
                }
                return null;
            }
        }

        /// <summary>
        /// A unified method to execute a Mediator request with exception handling.
        /// </summary>
        /// <typeparam name="TRequest">The type returned by the Mediator request</typeparam>
        /// <param name="request">The request to send to the Mediator</param>
        /// <returns>An ActionResult with the appropriate HTTP status code</returns>
        public async Task<ActionResult> APIExecute<TRequest>(IRequest<TRequest> request)
        {
            try
            {
                var response = await Mediator.Send(request);
                return Ok(response);
            }
            catch (NotFoundException ex)
            {
                return NotFound(new Response<int>
                {
                    Data = 0,
                    Message = ex.Message,
                    HttpStatusCode = HttpStatusCode.NotFound,
                    Succeeded = false
                });
            }
            catch (CustomValidationException ex)
            {
                return BadRequest(new Response<int>
                {
                    Data = 0,
                    Message = ex.Failures.First().Value.First(),
                    HttpStatusCode = HttpStatusCode.BadRequest,
                    Succeeded = false,
                    ModelErrors = ex.Failures
                });
            }
            catch (ForbiddenAccessException ex)
            {
                return BadRequest(new Response<int>
                {
                    Data = 0,
                    Message = ex.Message,
                    HttpStatusCode = HttpStatusCode.Forbidden,
                    Succeeded = false,
                });
            }
            catch (Exception ex)
            {
                return BadRequest(new Response<int>
                {
                    Data = 0,
                    Message = ex.Message,
                    HttpStatusCode = HttpStatusCode.BadRequest,
                    Succeeded = false
                });
            }
        }
    }
}
