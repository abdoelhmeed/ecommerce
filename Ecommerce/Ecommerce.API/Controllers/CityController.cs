﻿using Ecommerce.Application.Dtos.CityDtos;
using Ecommerce.Application.Features.CityFeature.Commands.Create;
using Ecommerce.Application.Features.CityFeature.Commands.Delete;
using Ecommerce.Application.Features.CityFeature.Commands.Update;
using Ecommerce.Application.Features.CityFeature.Queries.Query;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Ecommerce.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CityController : BaseApiController<CityController>
    {
        // GET: api/<CityController>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] CityFilter filter)
        {
            return await APIExecute(new GetCityQuery(filter));
        }

        // GET api/<CityController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            return await APIExecute(new GetCityByIdQuery(CompanyId, id));
        }

        // POST api/<CityController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateCityDto model)
        {
            return await APIExecute(new CreateCityCommand(CurrentUserId, CompanyId, model));
        }

        // PUT api/<CityController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] UpdateCityDto model)
        {
            return await APIExecute(new UpdateCityCommand(CurrentUserId, CompanyId, id, model));

        }

        // DELETE api/<CityController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            return await APIExecute(new DeleteCityCommand(CurrentUserId, CompanyId, id));

        }
    }
}
