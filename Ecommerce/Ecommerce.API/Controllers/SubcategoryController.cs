﻿using Ecommerce.Application.Dtos.SubcategoryDtos;
using Ecommerce.Application.Features.SubcategoryFeature.Commands.Create;
using Ecommerce.Application.Features.SubcategoryFeature.Commands.Delete;
using Ecommerce.Application.Features.SubcategoryFeature.Commands.Update;
using Ecommerce.Application.Features.SubcategoryFeature.Queries.Query;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Ecommerce.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubcategoryController : BaseApiController<SubcategoryController>
    {
        // GET: api/<SubcategoryController>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] SubcategoryFilter filter)
        {
            return await APIExecute(new GetSubcategoryQuery(filter));
        }

        // GET api/<SubcategoryController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            return await APIExecute(new GetSubcategoryByIdQuery(CompanyId, id));
        }

        // POST api/<SubcategoryController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateSubcategoryDto model)
        {
            return await APIExecute(new CreateSubcategoryCommand(CurrentUserId, CompanyId, model));
        }

        // PUT api/<SubcategoryController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] UpdateSubcategoryDto model)
        {
            return await APIExecute(new UpdateSubcategoryCommand(CurrentUserId, CompanyId, id, model));

        }

        // DELETE api/<SubcategoryController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            return await APIExecute(new DeleteSubcategoryCommand(CurrentUserId, CompanyId, id));

        }
    }
}
