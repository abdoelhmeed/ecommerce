﻿using Ecommerce.Application.Dtos.CatgoryDtos;
using Ecommerce.Application.Features.CategoryFeature.Commands.Create;
using Ecommerce.Application.Features.CategoryFeature.Commands.Delete;
using Ecommerce.Application.Features.CategoryFeature.Commands.Update;
using Ecommerce.Application.Features.CategoryFeature.Queries.Query;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Ecommerce.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatgoryController : BaseApiController<CatgoryController>
    {
        // GET: api/<CatgoryController>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] CatgoryFilter filter)
        {
            return await APIExecute(new GetCatgoryQuery(filter));
        }

        // GET api/<CatgoryController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            return await APIExecute(new GetCatgoryByIdQuery(CompanyId, id));
        }

        // POST api/<CatgoryController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateCatgoryDto model)
        {
            return await APIExecute(new CreateCatgoryCommand(CurrentUserId, CompanyId, model));
        }

        // PUT api/<CatgoryController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] UpdateCatgoryDto model)
        {
            return await APIExecute(new UpdateCatgoryCommand(CurrentUserId, CompanyId, id, model));

        }

        // DELETE api/<CatgoryController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            return await APIExecute(new DeleteCatgoryCommand(CurrentUserId, CompanyId, id));

        }
    }
}
