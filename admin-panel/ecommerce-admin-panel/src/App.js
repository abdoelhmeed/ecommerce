import './App.css';
import {Routes,Router,Route} from 'react-router-dom'
import AdminLayout from './layouts/adminLayout/AdminLayout'
import NotFoundPage  from './pages/NotFoundPage'
import {HomePage} from './pages/homePage/HomePage'
import AuthLayout  from './layouts/authLayout/AuthLayout'
import Login from './pages/authPage/Login'

function App() {
  return (
    <Router>
    <Routes>
      <Route element={<AdminLayout />}>
        <Route path="/" element={<HomePage />} />
      </Route>
      <Route element={<AuthLayout />}>
        <Route path="/user" element={<Login />} />
      </Route>
      <Route path="*" element={<NotFoundPage />} />
    </Routes>
  </Router>
  );
}

export default App;


