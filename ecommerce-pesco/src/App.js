import "./App.css";
import HeaderComponent from "./components/layout/HeaderComponent";
import FooterComponent from "./components/layout/FooterComponent";
import { HeroComponent } from "./components/homePage/HeroComponent";
import divider from "./assets/images/divider.png";
import CategorySliderComponent from "./components/homePage/CategorySliderComponent";
import OurFeaturesCollectionComponent from "./components/homePage/OurFeaturesCollectionComponent";
import BannerComponent from "./components/homePage/BannerComponent";
import TrendProductComponent from "./components/homePage/TrendProductComponent";
import BlogComponent from "./components/homePage/BlogComponent";

function App() {
  return (
    <>
      <div>
        <HeaderComponent />
        {/*====== Main Bg  ======*/}
        <main className="main-bg">
          {/*====== Start Hero Section ======*/}
          <HeroComponent></HeroComponent>
          {/*====== End Hero Section ======*/}
          {/*====== Start Animated-headline Section ======*/}
          <section className="animated-headline-area primary-dark-bg pt-25 pb-25">
            <div className="headline-wrap style-one">
              <span className="marquee-wrap">
                <span className="marquee-inner left">
                  <span className="marquee-item">
                    <b>Women</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Shirts</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Jackets</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Jeans</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Blazer</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Men</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Jackets</b>
                    <i className="fas fa-bahai" />
                  </span>
                </span>
                <span className="marquee-inner left">
                  <span className="marquee-item">
                    <b>Women</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Shirts</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Jackets</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Jeans</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Blazer</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Men</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Jackets</b>
                    <i className="fas fa-bahai" />
                  </span>
                </span>
                <span className="marquee-inner left">
                  <span className="marquee-item">
                    <b>Women</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Shirts</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Jackets</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Jeans</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Blazer</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Men</b>
                    <i className="fas fa-bahai" />
                  </span>
                  <span className="marquee-item">
                    <b>Jackets</b>
                    <i className="fas fa-bahai" />
                  </span>
                </span>
              </span>
            </div>
          </section>
          {/*====== End Animated-headline Section ======*/}
          {/*====== Start Features Section ======*/}
          <section className="features-section pt-130">
            <div className="container">
              <div className="row">
                <div className="col-lg-12">
                  {/*=== Features Wrapper ===*/}
                  <div
                    className="features-wrapper"
                    data-aos="fade-up"
                    data-aos-delay={10}
                    data-aos-duration={1000}
                  >
                    {/*=== Iconic Box Item ===*/}
                    <div className="iconic-box-item icon-left-box mb-25">
                      <div className="icon">
                        <i className="fas fa-shipping-fast" />
                      </div>
                      <div className="content">
                        <h5>Free Shipping</h5>
                        <p>
                          You get your items delivered without any extra cost.
                        </p>
                      </div>
                    </div>
                    {/*=== Divider ===*/}
                    <div className="divider mb-25">
                      <img src={divider} alt="divider" />
                    </div>
                    {/*=== Iconic Box Item ===*/}
                    <div className="iconic-box-item icon-left-box mb-25">
                      <div className="icon">
                        <i className="fas fa-microphone" />
                      </div>
                      <div className="content">
                        <h5>Great Support 24/7</h5>
                        <p>
                          Our customer support team is available around the
                          clock{" "}
                        </p>
                      </div>
                    </div>
                    {/*=== Divider ===*/}
                    <div className="divider mb-25">
                      <img src={divider} alt="divider" />
                    </div>
                    {/*=== Iconic Box Item ===*/}
                    <div className="iconic-box-item icon-left-box mb-25">
                      <div className="icon">
                        <i className="far fa-handshake" />
                      </div>
                      <div className="content">
                        <h5>Return Available</h5>
                        <p>
                          Making it easy to return any items if you're not
                          satisfied.
                        </p>
                      </div>
                    </div>
                    {/*=== Divider ===*/}
                    <div className="divider mb-25">
                      <img src={divider} alt="divider" />
                    </div>
                    {/*=== Iconic Box Item ===*/}
                    <div className="iconic-box-item icon-left-box mb-25">
                      <div className="icon">
                        <i className="fas fa-sack-dollar" />
                      </div>
                      <div className="content">
                        <h5>Secure Payment</h5>
                        <p>
                          Shop with confidence knowing that our secure payment
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/*====== End Features Section ======*/}
          {/*====== Start Category Section ======*/}
          <CategorySliderComponent />
          {/*====== End Category Section ======*/}
          {/*====== Start Banner Section ======*/}
          <BannerComponent />
          {/*====== End Banner Section ======*/}
          {/*====== Start Features Section ======*/}
          <OurFeaturesCollectionComponent />
          {/*====== End Features Section ======*/}

          {/*====== Start Working Section  ======*/}
          <section className="work-processing-section pt-30 pb-90">
            <div className="container">
              <div className="row">
                <div className="col-lg-12">
                  {/*=== Section Title  ===*/}
                  <div
                    className="section-title text-center mb-60"
                    data-aos="fade-up"
                    data-aos-delay={10}
                    data-aos-duration={800}
                  >
                    <div className="sub-heading d-inline-flex align-items-center">
                      <i className="flaticon-sparkler" />
                      <span className="sub-title">Work Processing</span>
                      <i className="flaticon-sparkler" />
                    </div>
                    <h2>How it Work processing</h2>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-xl-3 col-sm-6">
                  {/*=== Iconic Box Item  ===*/}
                  <div
                    className="iconic-box-item style-two mb-40"
                    data-aos="fade-up"
                    data-aos-duration={1000}
                  >
                    <div className="sn-number">01</div>
                    <div className="icon">
                      <i className="flaticon-searching" />
                    </div>
                    <div className="content">
                      <h6>Browsing &amp; Choosing</h6>
                      <p>
                        This is where customers visit your online store, browse
                        your products.
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-xl-3 col-sm-6">
                  {/*=== Iconic Box Item  ===*/}
                  <div
                    className="iconic-box-item style-two mb-40"
                    data-aos="fade-up"
                    data-aos-duration={1200}
                  >
                    <div className="sn-number">02</div>
                    <div className="icon">
                      <i className="flaticon-payment-method" />
                    </div>
                    <div className="content">
                      <h6>Checkout &amp; Payment</h6>
                      <p>
                        Once they have picked their items, customers proceed to
                        checkout.
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-xl-3 col-sm-6">
                  {/*=== Iconic Box Item  ===*/}
                  <div
                    className="iconic-box-item style-two mb-40"
                    data-aos="fade-up"
                    data-aos-duration={1400}
                  >
                    <div className="sn-number">03</div>
                    <div className="icon">
                      <i className="flaticon-currency" />
                    </div>
                    <div className="content">
                      <h6>Order Fulfillment</h6>
                      <p>
                        After the order is placed, it's sent to your fulfillment
                        team.
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-xl-3 col-sm-6">
                  {/*=== Iconic Box Item  ===*/}
                  <div
                    className="iconic-box-item style-two mb-40"
                    data-aos="fade-up"
                    data-aos-duration={1600}
                  >
                    <div className="sn-number">04</div>
                    <div className="icon">
                      <i className="flaticon-delivery" />
                    </div>
                    <div className="content">
                      <h6>Delivery to Customer</h6>
                      <p>
                        The packed order is then sent off with a shipping
                        carrier
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/*====== End Working Section  ======*/}
          {/*====== Start Trending Products Sections  ======*/}
          <TrendProductComponent />
          {/*====== End Trending Products Sections  ======*/}
          {/*====== Start Blog Sections  ======*/}
          <BlogComponent />
          {/*====== End Blog Sections  ======*/}
        </main>
        {/*====== Start Footer Main  ======*/}
        <FooterComponent />
        {/*====== End Footer Main  ======*/}
      </div>
    </>
  );
}

export default App;
