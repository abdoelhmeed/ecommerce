import React from 'react';
import logo from "../../assets/images/logo/logo-main.png";
import { Link } from 'react-router-dom';


function HeaderComponent() {
  return (
    <>
      {/*====== Preloader ======*/}

  {/*======  Start Overlay  ======*/}
  <div className="offcanvas__overlay" />
  {/*====== Start Sidemenu-wrapper-cart Area ======*/}
  <div className="sidemenu-wrapper-cart">
    <div className="sidemenu-content">
      <div className="widget widget-shopping-cart">
        <h4>My cart</h4>
        <div className="sidemenu-cart-close"><i className="far fa-times" /></div>
        <div className="widget-shopping-cart-content">
          <ul className="pesco-mini-cart-list">
            <li className="sidebar-cart-item">
              <Link to="/" className="remove-cart"><i className="far fa-trash-alt" /></Link>
              <Link to="/">
                <img src="assets/images/products/cart-1.jpg" alt="cart " />
                leggings with mesh panels
              </Link>
              <span className="quantity">1 × <span><span className="currency">$</span>940.00</span></span>
            </li>
            <li className="sidebar-cart-item">
              <Link to="/" className="remove-cart"><i className="far fa-trash-alt" /></Link>
              <Link to="/">
                <img src="assets/images/products/cart-2.jpg" alt="cart " />
                Summer dress with belt
              </Link>
              <span className="quantity">1 × <span><span className="currency">$</span>940.00</span></span>
            </li>
            <li className="sidebar-cart-item">
              <Link to="/" className="remove-cart"><i className="far fa-trash-alt" /></Link>
              <Link to="/">
                <img src="assets/images/products/cart-3.jpg" alt="cart " />
                Floral print sundress
              </Link>
              <span className="quantity">1 × <span><span className="currency">$</span>940.00</span></span>
            </li>
            <li className="sidebar-cart-item">
              <Link to="/" className="remove-cart"><i className="far fa-trash-alt" /></Link>
              <Link to="/">
                <img src="assets/images/products/cart-4.jpg" alt="cart " />
                Sheath Gown Red Colors
              </Link>
              <span className="quantity">1 × <span><span className="currency">$</span>940.00</span></span>
            </li>
          </ul>
          <div className="cart-mini-total">
            <div className="cart-total">
              <span><strong>Subtotal:</strong></span> <span className="amount">1 × <span><span className="currency">$</span>940.00</span></span>
            </div>
          </div>
          <div className="cart-button-box">
            <Link to="checkout.html" className="theme-btn style-one">Proceed to checkout</Link>
          </div>
        </div>
      </div>
    </div>
  </div>{/*====== End Sidemenu-wrapper-cart Area ======*/}
  {/*====== Start Header Section ======*/}
  <header className="header-area">
    {/*===  Search Header Main  ===*/}
    <div className="search-header-main">
      <div className="container">
        {/*===  Search Header Inner  ===*/}
        <div className="search-header-inner">
          {/*=== Site Branding  ===*/}
          <div className="site-branding">
            <Link to="index.html" className="brand-logo"><img src={logo} alt="Logo" /></Link>
          </div>
          {/*===  Product Search Category  ===*/}
          <div className="product-search-category">
            <form action="/">
              <select className="wide">
                <option>All Categories</option>
                <option>Man Shirts</option>
                <option>Denim Jeans</option>
                <option>Casual Suit</option>
                <option>Summer Dress</option>
                <option>Sweaters</option>
                <option>Jackets</option>
              </select>
              <div className="form-group">
                <input type="text" placeholder="Enter Search Products" />
                <button className="search-btn"><i className="far fa-search" /></button>
              </div>
            </form>
          </div>
          {/*===  Hotline Support  ===*/}
          <div className="hotline-support item-rtl">
            <div className="icon">
              <i className="flaticon-support" />
            </div>
            <div className="info">
              <span>24/7 Support</span>
              <h5><Link to="tel:+941234567894">+94 123 4567 894</Link></h5>
            </div>
          </div>
        </div>
      </div>
    </div>
    {/*===  Header Navigation  ===*/}
    <div className="header-navigation style-one">
      <div className="container">
        {/*=== Primary Menu ===*/}
        <div className="primary-menu">
          <div className="site-branding d-lg-none d-block">
            <Link to="index.html" className="brand-logo"><img src="assets/images/logo/logo-main.png" alt="Logo" /></Link>
          </div>
          {/*=== Nav Inner Menu ===*/}
          <div className="nav-inner-menu">
            {/*=== Main Category ===*/}
            <div className="main-categories-wrap d-none d-lg-block">
              <Link className="categories-btn-active" to="/">
                <span className="fas fa-list" /><span className="text">Products Category<i className="fas fa-angle-down" /></span>
              </Link>
              <div className="categories-dropdown-wrap categories-dropdown-active">
                <div className="categori-dropdown-item">
                  <ul>
                    <li>
                      <Link to="shops.html"> <img src="assets/images/icon/sweaters.png" alt="Sweaters" />Sweaters</Link>
                    </li>
                  </ul>
                </div>
                <div className="more_slide_open">
                  <div className="categori-dropdown-item">
                    <ul>
                      <li>
                        <Link to="/"><img src="assets/images/icon/jacket.png" alt="Jackets" />Jackets</Link>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="more_categories"><span className="icon" /> <span>Show more...</span></div>
              </div>
            </div>
            {/*=== Pesco Nav Main ===*/}
            <div className="pesco-nav-main">
              {/*=== Pesco Nav Menu ===*/}
              <div className="pesco-nav-menu">
                {/*=== Responsive Menu Search ===*/}
                <div className="nav-search mb-40 d-block d-lg-none">
                  <div className="form-group">
                    <input type="search" className="form_control" placeholder="Search Here" name="search" />
                    <button className="search-btn"><i className="far fa-search" /></button>
                  </div>
                </div>
                {/*=== Responsive Menu Tab ===*/}
                <div className="pesco-tabs style-three d-block d-lg-none">
                  <ul className="nav nav-tabs mb-30" role="tablist">
                    <li>
                      <button className="nav-link active" data-bs-toggle="tab" data-bs-target="#nav1" role="tab">Menu</button>
                    </li>
                    <li>
                      <button className="nav-link" data-bs-toggle="tab" data-bs-target="#nav2" role="tab">Category</button>
                    </li>
                  </ul>
                  <div className="tab-content">
                    <div className="tab-pane fade show active" id="nav1">
                      <nav className="main-menu">
                        <ul>
                          <li className="menu-item has-children"><Link to="/">Home</Link>
                            <ul className="sub-menu">
                              <li><Link to="index.html">Home 01</Link></li>
                              <li><Link to="index-2.html">Home 02</Link></li>
                            </ul>
                          </li>
                          <li className="menu-item has-children"><Link to="/">Shop</Link>
                            <ul className="sub-menu">
                              <li><Link to="shops-grid.html">Shop Grid</Link></li>
                              <li><Link to="shops.html">Shop left Sidebar</Link></li>
                              <li><Link to="shops-right-sidebar.html">Shop Right Sidebar</Link></li>
                              <li><Link to="shop-details.html">Product Details</Link></li>
                              <li><Link to="cart.html">Cart</Link></li>
                              <li><Link to="checkout.html">Checkout</Link></li>
                              <li><Link to="wishlists.html">Wishlist</Link></li>
                            </ul>
                          </li>
                          <li className="menu-item has-children"><Link to="/">Blog</Link>
                            <ul className="sub-menu">
                              <li><Link to="blogs.html">Our Blog</Link></li>
                              <li><Link to="blog-details.html">Blog Details</Link></li>
                            </ul>
                          </li>
                          <li className="menu-item has-children"><Link to="/">Pages</Link>
                            <ul className="sub-menu">
                              <li><Link to="about-us.html">About Us</Link></li>
                              <li><Link to="faq.html">Faqs</Link></li>
                            </ul>
                          </li>
                          <li className="menu-item"><Link to="contact.html">Contact</Link></li>
                        </ul>
                      </nav>
                    </div>
                    <div className="tab-pane fade" id="nav2">
                      <div className="categori-dropdown-item">
                        <ul>
                          <li>
                            <Link to="shops.html"> <img src="assets/images/icon/shirt.png" alt="Shirts" />Man Shirts</Link>
                          </li>
                          <li>
                            <Link to="shops.html"> <img src="assets/images/icon/denim.png" alt="Jeans" />Denim Jeans</Link>
                          </li>
                          <li>
                            <Link to="shops.html"> <img src="assets/images/icon/suit.png" alt="Suit" />Casual Suit</Link>
                          </li>
                          <li>
                            <Link to="shops.html"> <img src="assets/images/icon/dress.png" alt="Dress" />Summer Dress</Link>
                          </li>
                          <li>
                            <Link to="shops.html"> <img src="assets/images/icon/sweaters.png" alt="Sweaters" />Sweaters</Link>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                {/*===  Hotline Support  ===*/}
                <div className="hotline-support d-flex d-lg-none mt-30">
                  <div className="icon">
                    <i className="flaticon-support" />
                  </div>
                  <div className="info">
                    <span>24/7 Support</span>
                    <h5><Link to="tel:+941234567894">+94 123 4567 894</Link></h5>
                  </div>
                </div>
                {/*=== Main Menu ===*/}
                <nav className="main-menu d-none d-lg-block">
                  <ul>
                    <li className="menu-item has-children"><Link to="/">Home</Link>
                      <ul className="sub-menu">
                        <li><Link to="index.html">Home 01</Link></li>
                        <li><Link to="index-2.html">Home 02</Link></li>
                      </ul>
                    </li>
                    <li className="menu-item has-children"><Link to="/">Shop</Link>
                      <ul className="sub-menu">
                        <li><Link to="shops-grid.html">Shop Grid</Link></li>
                        <li><Link to="shops.html">Shop left Sidebar</Link></li>
                        <li><Link to="shops-right-sidebar.html">Shop Right Sidebar</Link></li>
                        <li><Link to="shop-details.html">Product Details</Link></li>
                        <li><Link to="cart.html">Cart</Link></li>
                        <li><Link to="checkout.html">Checkout</Link></li>
                        <li><Link to="wishlists.html">Wishlist</Link></li>
                      </ul>
                    </li>
                    <li className="menu-item has-children"><Link to="/">Blog</Link>
                      <ul className="sub-menu">
                        <li><Link to="blogs.html">Our Blog</Link></li>
                        <li><Link to="blog-details.html">Blog Details</Link></li>
                      </ul>
                    </li>
                    <li className="menu-item has-children"><Link to="/">Pages</Link>
                      <ul className="sub-menu">
                        <li><Link to="about-us.html">About Us</Link></li>
                        <li><Link to="faq.html">Faqs</Link></li>
                      </ul>
                    </li>
                    <li className="menu-item"><Link to="contact.html">Contact</Link></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
          {/*=== Nav Right Item ===*/}
          <div className="nav-right-item style-one">
            <ul>
              <li>
                <div className="deals d-lg-block d-none"><i className="far fa-fire-alt" />Deal</div>
              </li>
              <li>
                <div className="wishlist-btn d-lg-block d-none"><i className="far fa-heart" /><span className="pro-count">12</span></div>
              </li>
              <li>
                <div className="cart-button d-flex align-items-center">
                  <div className="icon">
                    <i className="fas fa-shopping-bag" /><span className="pro-count">01</span>
                  </div>
                </div>
              </li>
            </ul>
            <div className="navbar-toggler d-block d-lg-none">
              <span />
              <span />
              <span />
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>{/*====== End Header Section ======*/}
    
    </>
  )
}

export default HeaderComponent