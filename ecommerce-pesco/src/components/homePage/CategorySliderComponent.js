import React from 'react';
import Slider from "react-slick";
import category from "../../assets/images/category/category-2.png"


function CategorySliderComponent() {
  var settings = {
    dots: false,
    infinite: true,
    speed: 1000,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows:true
  };
  return (
    <section className="category-section pt-125 overflow-hidden">
    <div className="container">
      <div className="row align-items-center">
        <div className="col-lg-6 col-md-8">
          {/*=== Section Title ===*/}
          <div
            className="section-title mb-50"
            data-aos="fade-right"
            data-aos-delay={10}
            data-aos-duration={800}
          >
            <div className="sub-heading d-inline-flex align-items-center">
              <i className="flaticon-sparkler" />
              <span className="sub-title">Categories</span>
            </div>
            <h2>Browse Top Category</h2>
          </div>
        </div>
        <div className="col-lg-6 col-md-4">
          {/*=== Arrows ===*/}
          <div
            className="category-arrows style-one mb-60"
            data-aos="fade-left"
            data-aos-delay={15}
            data-aos-duration={1000}
          />
        </div>
      </div>
    </div>
    {/*=== Category Slider ===*/}
    <Slider  {...settings}
      className="category-slider-one"
      data-aos="fade-up"
      data-aos-delay={20}
      data-aos-duration={1200} >
      {/*=== Category Item ===*/}
      <div className="category-item style-one text-center">
        <div className="category-img">
          <img
            src={category}
            alt="category image"
          />
        </div>
        <div className="category-content">
          <a href="index.html" className="category-btn">
            Man Shirts
          </a>
        </div>
      </div>
      {/*=== Category Item ===*/}
      <div className="category-item style-one text-center">
        <div className="category-img">
          <img
            src={category}
            alt="category image"
          />
        </div>
        <div className="category-content">
          <a href="index.html" className="category-btn">
            Denim Jeans y
          </a>
        </div>
      </div>
      <div className="category-item style-one text-center">
        <div className="category-img">
          <img
            src={category}
            alt="category image"
          />
        </div>
        <div className="category-content">
          <a href="index.html" className="category-btn">
             Jeans8
          </a>
        </div>
      </div>
      <div className="category-item style-one text-center">
        <div className="category-img">
          <img
            src={category}
            alt="category image"
          />
        </div>
        <div className="category-content">
          <a href="index.html" className="category-btn">
             Jeans L
          </a>
        </div>
      </div>
      <div className="category-item style-one text-center">
        <div className="category-img">
          <img
            src={category}
            alt="category image"
          />
        </div>
        <div className="category-content">
          <a href="index.html" className="category-btn">
            Denim Jeans MM
          </a>
        </div>
      </div>
    </Slider>
  </section>
  )
}

export default CategorySliderComponent