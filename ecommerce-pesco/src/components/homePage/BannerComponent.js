import React from 'react';
import discount from "../../assets/images/banner/discount.png";
import line from "../../assets/images/banner/line.png";
import banner from "../../assets/images/banner/banner-1.png"

function BannerComponent() {
  return (
    <section className="banner-section pt-130">
    <div className="container">
      <div className="row">
        <div className="col-lg-6">
          {/*=== Banner Item ===*/}
          <div
            className="banner-item style-one bg-one mb-40"
            data-aos="fade-up"
            data-aos-delay={10}
            data-aos-duration={900}
          >
            <div className="shape shape-one">
              <span>
                <img
                  src={discount}
                  alt="shape"
                />
              </span>
            </div>
            <div className="shape shape-two">
              <span>
                <img src={line} alt="shape" />
              </span>
            </div>
            <div className="banner-img">
              <img
                src={banner}
                alt="banner image"
              />
            </div>
            <div className="banner-content">
              <span>
                UP TO <span className="off">50%</span>
              </span>
              <h4>Exclusive Kids &amp; Adults Summer Outfits</h4>
              <a href="shops.html" className="theme-btn style-one">
                Shop Now
              </a>
            </div>
          </div>
        </div>
        <div className="col-lg-6">
          {/*=== Banner Item ===*/}
          <div
            className="banner-item style-one bg-two mb-40"
            data-aos="fade-up"
            data-aos-delay={20}
            data-aos-duration={1100}
          >
            <div className="shape shape-one">
              <span>
                <img
                  src={discount}
                  alt="shape"
                />
              </span>
            </div>
            <div className="shape shape-two">
              <span>
                <img src={line} alt="shape" />
              </span>
            </div>
            <div className="banner-img">
              <img
                src={banner}
                alt="banner image"
              />
            </div>
            <div className="banner-content">
              <span>
                UP TO <span className="off">70%</span>
              </span>
              <h4>Exclusive Kids &amp; Adults Summer Outfits</h4>
              <a href="shops.html" className="theme-btn style-one">
                Shop Now
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  )
}

export default BannerComponent