import React from "react";
import Slider from "react-slick";
import line1 from "../../assets/images/hero/line-1.png";
import one_img1 from "../../assets/images/hero/hero-one_img1.jpg";
import { Link } from "react-router-dom";

export const HeroComponent = () => {
  var settings = {
    dots: false,
    infinite: true,
    speed: 3000,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows:true
  };
  return (
    <section className="hero-section">
      {/*=== Hero Wrapper ===*/}
      <div className="hero-wrapper-one">
        <div className="container">
          <div className="hero-dots" />
          <Slider {...settings} className="hero-slider-one">
            {/*=== Single Slider ===*/}
            <div className="single-hero-slider">
              <div className="row align-items-center">
                <div className="col-lg-6">
                  {/*=== Hero Content ===*/}
                  <div className="hero-content style-one mb-50">
                    <span className="sub-heading">
                      Best for your categories
                    </span>
                    <h1>
                      Exclusive Collection <br />
                      in <span>Our Online</span> Store
                    </h1>
                    <p>
                      Discover our exclusive collection available only in our
                      online store. Shop now for unique and premium items that
                      you won't find anywhere else.
                    </p>
                    <ul>
                      <li>
                        <div className="price-box">
                          <div className="currency">$</div>
                          <div className="text">
                            <span className="discount">Discount Price</span>
                            <h3>140.00</h3>
                          </div>
                        </div>
                      </li>
                      <li>
                        <img src={line1} alt="...." />
                      </li>
                      <li>
                        <Link to="shops.html" className="theme-btn style-one">
                          Shop Now
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col-lg-6">
                  {/*=== Hero Image ===*/}
                  <div className="hero-image-box">
                    <div className="hero-image">
                      <img src={one_img1} alt="...." />
                      <div
                        className="hero-shape bg_cover"
                        style={{
                          backgroundImage:
                            "url(../../assets/images/hero/hero-one-shape1.png)",
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/*=== Single Slider ===*/}
            <div className="single-hero-slider">
              <div className="row align-items-center">
                <div className="col-lg-6">
                  {/*=== Hero Content ===*/}
                  <div className="hero-content style-one mb-50">
                    <span className="sub-heading">
                      Best for your categories
                    </span>
                    <h1>
                      Exclusive Collection <br />
                      in <span>Our Online</span> Store
                    </h1>
                    <p>
                      Discover our exclusive collection available only in our
                      online store. Shop now for unique and premium items that
                      you won't find anywhere else.
                    </p>
                    <ul>
                      <li>
                        <div className="price-box">
                          <div className="currency">$</div>
                          <div className="text">
                            <span className="discount">Discount Price</span>
                            <h3>140.00</h3>
                          </div>
                        </div>
                      </li>
                      <li>
                        <img src={line1} alt="........" />
                      </li>
                      <li>
                        <Link to="shops.html" className="theme-btn style-one">
                          Shop Now
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col-lg-6">
                  {/*=== Hero Image ===*/}
                  <div className="hero-image-box">
                    <div className="hero-image">
                      <img src={one_img1} alt="....." />
                      <div
                        className="hero-shape bg_cover"
                        style={{
                          backgroundImage:
                            "url(assets/images/hero/hero-one-shape1.png)",
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/*=== Single Slider ===*/}
            <div className="single-hero-slider">
              <div className="row align-items-center">
                <div className="col-lg-6">
                  {/*=== Hero Content ===*/}
                  <div className="hero-content style-one mb-50">
                    <span className="sub-heading">
                      Best for your categories
                    </span>
                    <h1>
                      Exclusive Collection <br />
                      in <span>Our Online</span> Store
                    </h1>
                    <p>
                      Discover our exclusive collection available only in our
                      online store. Shop now for unique and premium items that
                      you won't find anywhere else.
                    </p>
                    <ul>
                      <li>
                        <div className="price-box">
                          <div className="currency">$</div>
                          <div className="text">
                            <span className="discount">Discount Price</span>
                            <h3>140.00</h3>
                          </div>
                        </div>
                      </li>
                      <li>
                        <img src={line1} alt="..........." />
                      </li>
                      <li>
                        <Link to="shops.html" className="theme-btn style-one">
                          Shop Now
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="col-lg-6">
                  {/*=== Hero Image ===*/}
                  <div className="hero-image-box">
                    <div className="hero-image">
                      <img src={one_img1} alt="...." />
                      <div
                        className="hero-shape bg_cover"
                        style={{
                          backgroundImage:
                            "url(../../assets/images/hero/hero-one-shape1.png)",
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Slider>
        </div>
      </div>
    </section>
  );
};
