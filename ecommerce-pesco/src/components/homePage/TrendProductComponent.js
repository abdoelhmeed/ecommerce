import React from "react";
import trendingproduct from "../../assets/images/products/trending-product-2.png";
import Slider from "react-slick";
import { Link } from "react-router-dom";


function TrendProductComponent() {
  var settings = {
    dots: false,
    infinite: true,
    speed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows:true
  };
  return (
    <section className="trending-products-section pb-40 pb-130">
      <div className="container">
        <div className="row">
          <div className="col-md-8">
            {/*=== Section Title  ===*/}
            <div
              className="section-title mb-50"
              data-aos="fade-right"
              data-aos-duration={1000}
            >
              <div className="sub-heading d-inline-flex align-items-center">
                <i className="flaticon-sparkler" />
                <span className="sub-title">Trending Products</span>
              </div>
              <h2>What's Trending Now</h2>
            </div>
          </div>
          <div className="col-md-4">
            {/*=== Arrows ===*/}
            <div
              className="trending-product-arrows style-one mb-60"
              data-aos="fade-left"
              data-aos-duration={1200}
            />
          </div>
        </div>
      </div>
      <div className="container-fluid">
        <Slider
          {...settings}
          className="trending-products-slider"
          data-aos="fade-up"
          data-aos-duration={1400}
        >
          {/*=== Product Item ===*/}
          <div className="product-item style-two">
            <div className="product-thumbnail">
              <img src={trendingproduct} alt="Products" />
            </div>
            <div className="product-info-wrap">
              <div className="product-info">
                <ul className="ratings rating5">
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <Link to="/">(80)</Link>
                  </li>
                </ul>
                <h4 className="title">
                  <Link href="shop-details.html">
                    Women Red &amp; White Striped Crepe Top
                  </Link>
                </h4>
              </div>
              <div className="product-price">
                <span className="price prev-price">
                  <span className="currency">$</span>90.00
                </span>
                <span className="price new-price">
                  <span className="currency">$</span>10.00
                </span>
              </div>
            </div>
          </div>
          {/*=== Product Item ===*/}
          <div className="product-item style-two">
            <div className="product-thumbnail">
              <img src={trendingproduct} alt="Products" />
            </div>
            <div className="product-info-wrap">
              <div className="product-info">
                <ul className="ratings rating5">
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <Link to="/">(80)</Link>
                  </li>
                </ul>
                <h4 className="title">
                  <Link href="shop-details.html">Cozy knit sweater with pockets</Link>
                </h4>
              </div>
              <div className="product-price">
                <span className="price prev-price">
                  <span className="currency">$</span>67.00
                </span>
                <span className="price new-price">
                  <span className="currency">$</span>40.00
                </span>
              </div>
            </div>
          </div>
          {/*=== Product Item ===*/}
          <div className="product-item style-two">
            <div className="product-thumbnail">
              <img src={trendingproduct} alt="Products" />
            </div>
            <div className="product-info-wrap">
              <div className="product-info">
                <ul className="ratings rating5">
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <Link to="/">(80)</Link>
                  </li>
                </ul>
                <h4 className="title">
                  <Link href="shop-details.html">
                    Striped cotton t-shirt with crew neck
                  </Link>
                </h4>
              </div>
              <div className="product-price">
                <span className="price prev-price">
                  <span className="currency">$</span>59.00
                </span>
                <span className="price new-price">
                  <span className="currency">$</span>47.00
                </span>
              </div>
            </div>
          </div>
          {/*=== Product Item ===*/}
          <div className="product-item style-two">
            <div className="product-thumbnail">
              <img src={trendingproduct} alt="Products" />
            </div>
            <div className="product-info-wrap">
              <div className="product-info">
                <ul className="ratings rating5">
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <Link to="/">(80)</Link>
                  </li>
                </ul>
                <h4 className="title">
                  <Link href="shop-details.html">
                    Cashmere v-neck sweater with ribbed cuffs
                  </Link>
                </h4>
              </div>
              <div className="product-price">
                <span className="price prev-price">
                  <span className="currency">$</span>67.00
                </span>
                <span className="price new-price">
                  <span className="currency">$</span>20.00
                </span>
              </div>
            </div>
          </div>
          {/*=== Product Item ===*/}
          <div className="product-item style-two">
            <div className="product-thumbnail">
              <img src={trendingproduct} alt="Products" />
            </div>
            <div className="product-info-wrap">
              <div className="product-info">
                <ul className="ratings rating5">
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <Link to="/">(80)</Link>
                  </li>
                </ul>
                <h4 className="title">
                  <Link href="shop-details.html">
                    Quilted puffer vest with faux fur collar
                  </Link>
                </h4>
              </div>
              <div className="product-price">
                <span className="price prev-price">
                  <span className="currency">$</span>90.00
                </span>
                <span className="price new-price">
                  <span className="currency">$</span>10.00
                </span>
              </div>
            </div>
          </div>
          {/*=== Product Item ===*/}
          <div className="product-item style-two">
            <div className="product-thumbnail">
              <img src={trendingproduct} alt="Products" />
            </div>
            <div className="product-info-wrap">
              <div className="product-info">
                <ul className="ratings rating5">
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <i className="fas fa-star" />
                  </li>
                  <li>
                    <Link to="/">(80)</Link>
                  </li>
                </ul>
                <h4 className="title">
                  <Link href="shop-details.html">
                    Cashmere v-neck sweater with ribbed cuffs
                  </Link>
                </h4>
              </div>
              <div className="product-price">
                <span className="price prev-price">
                  <span className="currency">$</span>67.00
                </span>
                <span className="price new-price">
                  <span className="currency">$</span>20.00
                </span>
              </div>
            </div>
          </div>
        </Slider>
      </div>
    </section>
  );
}

export default TrendProductComponent;
