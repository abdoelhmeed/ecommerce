import React from 'react';
import deal from "../../assets/images/banner/deal-1.png";
import { Link } from 'react-router-dom';

function BlogComponent() {
  return (
    <section className="best-deal-section">
    <div className="container">
      <div className="row">
        <div className="col-lg-12">
          <div
            className="offer-deal-wrapper bg_cover"
            data-aos="fade-up"
            data-aos-duration={1400}
            style={{
              backgroundImage: "url(assets/images/bg/deal-bg-1.png)",
            }}
          >
            <div className="deal-img">
              <span>
                <img
                  src={deal}
                  alt="deals"
                />
              </span>
            </div>
            <div className="deal-content">
              <span className="sub-heading">
                <i className="fas fa-tags" />
                Deal of the Week
              </span>
              <h2>
                Hurry Up! Offer ends in. Get <span>UP TO 80% OFF</span>
              </h2>
              <div className="simply-countdown mb-60" />
              <div className="shop-button">
                <Link to="/" className="theme-btn style-one">
                  Shop Now
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  )
}

export default BlogComponent