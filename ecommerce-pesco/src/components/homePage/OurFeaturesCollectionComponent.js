import React from 'react'
import product from "../../assets/images/products/feature-product-1.png";
import { Link } from 'react-router-dom';


function OurFeaturesCollectionComponent() {
  return (
      <section className="features-products pt-90">
            <div className="container">
              <div className="row">
                <div className="col-lg-6">
                  {/*=== Section Title ===*/}
                  <div
                    className="section-title mb-50 text-center text-lg-start"
                    data-aos="fade-right"
                    data-aos-delay={10}
                    data-aos-duration={1000}
                  >
                    <div className="sub-heading d-inline-flex align-items-center">
                      <i className="flaticon-sparkler" />
                      <span className="sub-title">Feature Products</span>
                    </div>
                    <h2>Our Features Collection</h2>
                  </div>
                </div>
                <div className="col-lg-6">
                  {/*=== Pesco Tabs ===*/}
                  <div
                    className="pesco-tabs style-one mb-50"
                    data-aos="fade-left"
                    data-aos-delay={15}
                    data-aos-duration={1200}
                  >
                    <ul className="nav nav-tabs" role="tablist">
                      <li>
                        <button
                          className="nav-link active"
                          data-bs-toggle="tab"
                          data-bs-target="#cat1"
                          role="tab"
                        >
                          Best Sellers
                        </button>
                      </li>
                      <li>
                        <button
                          className="nav-link"
                          data-bs-toggle="tab"
                          data-bs-target="#cat2"
                          role="tab"
                        >
                          New Products
                        </button>
                      </li>
                      <li>
                        <button
                          className="nav-link"
                          data-bs-toggle="tab"
                          data-bs-target="#cat3"
                          role="tab"
                        >
                          Sale Products
                        </button>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12">
                  {/*=== Tab Content ===*/}
                  <div
                    className="tab-content"
                    data-aos="fade-up"
                    data-aos-duration={1200}
                  >
                    {/*=== Tab Pane  ===*/}
                    <div className="tab-pane fade show active" id="cat1">
                      <div className="row justify-content-center">
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">10% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Lightweight linen summer dress with belt
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>80.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>40.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">40% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Cozy knit sweater with pockets
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>67.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>23.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">10% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Athletic leggings with mesh panels
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>80.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>40.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">40% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Classic leather biker jacket with zippers
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>67.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>23.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">80% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Floral print sundress with adjustable straps
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>80.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>40.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">40% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Relaxed fit denim jeans with distressing
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>67.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>23.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">40% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Cargo shorts with pockets and drawstring
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>80.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>40.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">80% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Elegant silk dress with sequins
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>67.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>23.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="tab-pane fade" id="cat2">
                      <div className="row justify-content-center">
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">10% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Lightweight linen summer dress with belt
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>80.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>40.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">40% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Cozy knit sweater with pockets
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>67.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>23.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">10% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Athletic leggings with mesh panels
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>80.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>40.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">40% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Classic leather biker jacket with zippers
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>67.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>23.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">80% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Floral print sundress with adjustable straps
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>80.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>40.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">40% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Relaxed fit denim jeans with distressing
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>67.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>23.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">40% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Cargo shorts with pockets and drawstring
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>80.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>40.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">80% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Elegant silk dress with sequins
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>67.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>23.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="tab-pane fade" id="cat3">
                      <div className="row justify-content-center">
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">10% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Lightweight linen summer dress with belt
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>80.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>40.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">40% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Cozy knit sweater with pockets
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>67.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>23.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">10% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Athletic leggings with mesh panels
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>80.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>40.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">40% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Classic leather biker jacket with zippers
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>67.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>23.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">80% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Floral print sundress with adjustable straps
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>80.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>40.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-3 col-lg-4 col-sm-6">
                          {/*=== Product Item  ===*/}
                          <div className="product-item style-one mb-40">
                            <div className="product-thumbnail">
                              <img
                                src={product}
                                alt="Products"
                              />
                              <div className="discount">40% Off</div>
                              <div className="hover-content">
                                <Link to="/" className="icon-btn">
                                  <i className="fa fa-heart" />
                                </Link>
                                <Link
                                  href={product}
                                  className="img-popup icon-btn"
                                >
                                  <i className="fa fa-eye" />
                                </Link>
                              </div>
                              <div className="cart-button">
                                <Link to="/" className="cart-btn">
                                  <i className="far fa-shopping-basket" />{" "}
                                  <span className="text">Add To Cart</span>
                                </Link>
                              </div>
                            </div>
                            <div className="product-info-wrap">
                              <div className="product-info">
                                <ul className="ratings rating5">
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <i className="fas fa-star" />
                                  </li>
                                  <li>
                                    <Link to="/">(80)</Link>
                                  </li>
                                </ul>
                                <h4 className="title">
                                  <Link href="shop-details.html">
                                    Relaxed fit denim jeans with distressing
                                  </Link>
                                </h4>
                              </div>
                              <div className="product-price">
                                <span className="price prev-price">
                                  <span className="currency">$</span>67.00
                                </span>
                                <span className="price new-price">
                                  <span className="currency">$</span>23.00
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
  )
}

export default OurFeaturesCollectionComponent